#include<stdio.h>
#include<gsl/gsl_odeiv2.h>
#include<math.h>
#include<gsl/gsl_errno.h>

int sine_ode(double x, const double y[], double dydx[], void* params){
	dydx[0] = y[1];
	dydx[1] = -y[0];
	
return GSL_SUCCESS;
}

double mysine(double x){
	gsl_odeiv2_system sys;
	sys.function = sine_ode;
	sys.jacobian = NULL;
	sys.dimension = 2;
	sys.params = NULL;
	
	double hstart = copusine(0.1,x);
	double acc = 1e-6;
	double eps = 1e-6;

	gsl_odeiv2_driver* driver = 
		gsl_odeiv2_driver_alloc_v_new
			(&sys,gsl_odeiv2_step_rkf45,hstart, acc, eps);
	
	double t = 0;
	double y[2] = {0,1};
	gsl_odeiv2_driver_apply(driver, &t,x,y);

	gsl_odeiv2_driver_free(driver);
	return y[0];
}

int main(){
	for(double x = 2*M_PI;x<2*M_PI;x += 0.1);	
	printf("%g %g %g\n",x,my_sine(x),sin(x));
	for(double x = 2*M_PI;x<2*M_PI;x += 0.1);	

}
