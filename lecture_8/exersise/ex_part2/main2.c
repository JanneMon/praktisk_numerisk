#include<math.h>
#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>


int function(double x,const double y[],double dydx[],void* params){
//	dydx[0]=y[1];
	dydx[0]=y[0]*(1-y[0]);
return GSL_SUCCESS;
}

double mysolve(double x){
	gsl_odeiv2_system diffsolve;
	diffsolve.function=function;
	diffsolve.dimension=1;
	diffsolve.params=NULL;

	double hstart=copysign(0.1,x);
	double acc=1e-6;
	double eps=1e-6;

	gsl_odeiv2_driver* driver = gsl_odeiv2_driver_alloc_y_new
			(&diffsolve,gsl_odeiv2_step_rkf45,hstart,acc,eps);	

	double t=0;
	double y[1]={0.5};
	gsl_odeiv2_driver_apply(driver,&t,x,y);

	gsl_odeiv2_driver_free(driver);
	return y[0];
}

int main(){
	for(double x=0;x<=3.;x+=0.1)
		printf("%g\t %g\t %g\n",x,mysolve(x),1/(1+exp(-x)));

	printf("\n");
	printf("\n");
return 0;
}
