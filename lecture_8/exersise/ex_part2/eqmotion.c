#include<math.h>
#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>


int orbit_solver(double x,const double y[],double dydx[],void* params){
	double epsilon = *(double*) params;
	dydx[0]=y[1];
	dydx[1]=1-y[0]+epsilon*y[0]*y[0];
return GSL_SUCCESS;
}

double myorbit(double x, double epsilon, double value){
	gsl_odeiv2_system orbit;
	orbit.function=orbit_solver;
	orbit.jacobian=NULL;
	orbit.dimension=2;
	orbit.params=(void*) &epsilon;

	double hstart=1e-3;
	double acc=1e-6;
	double eps=1e-6;
	gsl_odeiv2_driver* driver =
		gsl_odeiv2_driver_alloc_y_new
			(&orbit,gsl_odeiv2_step_rk8pd,hstart,acc,eps);	

	double t=0;
	double y[2]={1,value};
	gsl_odeiv2_driver_apply(driver,&t,x,y);

	gsl_odeiv2_driver_free(driver);
	return y[0];
}

int main(){
	for(double x=0;x<=2*M_PI+1.;x+=0.1)
		printf("%g\t %g\n",x,myorbit(x,0,0 ));
	printf("\n\n");
	for(double x=0;x<=2*M_PI+1.;x+=0.1)
		printf("%g\t %g\n",x,myorbit(x,0,-0.5));
	printf("\n\n");
	for(double x=0;x<=2*M_PI+1.;x+=0.1)
		printf("%g\t %g\n",x,myorbit(x,0.1,-0.5));
return 0;
}
