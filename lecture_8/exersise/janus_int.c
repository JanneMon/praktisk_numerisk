#include<tgmath.h>
#include<gsl/gsl_integration.h> 
#include<stdio.h>

double integrand(double x, void* params){
	// double z= *(double*)params;
	return log(x)/sqrt(x);
}

double int_my(double z){
	
	// define gsl_function struct
	gsl_function funct;
	funct.function=&integrand;
	funct.params= (void*)&z;
	
	double acc = 1e-6; 	// accuracy
	double eps = 1e-6; 	// epsilon - Konvergensgr�nse
	double result;	   	// empty space for pointer
	double err;		   	// --- " " ---
	size_t limit=100;
	
	// define workspace
	gsl_integration_workspace* workspace= gsl_integration_workspace_alloc(limit);
	
	// Call integrationfunction - free workspace
	gsl_integration_qagiu(&funct, 0.1, acc, eps, limit, workspace, &result, &err);
	gsl_integration_workspace_free(workspace);
	
	return result;
}
