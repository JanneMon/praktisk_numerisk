#include<stdio.h>
#include<gsl/gsl_sf_airy.h>
#include<math.h>
#include<gsl/gsl_sf.h>
#include<stdlib.h>
#define mode GSL_PREC_APPROX

int main(int argc, char** argv){
	
	for(int i = 1; i<argc; i++){
		double x = atof(argv[i]);
		printf("%g \t %g \t %g \n",x,gsl_sf_airy_Ai(x,mode),gsl_sf_airy_Bi(x,mode));}
return 0;
}
