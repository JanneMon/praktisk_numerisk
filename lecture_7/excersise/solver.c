#include<gsl/gsl_linalg.h>
#include<gsl/gsl_blas.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<stdio.h>
#include<stdlib.h>
#include<math.h>

int main(){

	gsl_matrix* A = gsl_matrix_alloc(3,3);
	
	gsl_matrix_set(A,0,0,6.13);
	gsl_matrix_set(A,1,0,8.08);
	gsl_matrix_set(A,2,0,-4.35);

	gsl_matrix_set(A,0,1,-2.90);
	gsl_matrix_set(A,1,1,-6.31);
	gsl_matrix_set(A,2,1,1.00);

	gsl_matrix_set(A,0,2,5.86);
	gsl_matrix_set(A,1,2,-3.89);
	gsl_matrix_set(A,2,2,0.19);

	gsl_vector* b = gsl_vector_alloc(3);

	gsl_vector_set(b,0,6.23);
	gsl_vector_set(b,1,5.37);
	gsl_vector_set(b,2,2.29);
	
	
	gsl_vector* x = gsl_vector_alloc(3);//empty matrix needed//

	gsl_matrix* K = gsl_matrix_alloc(3,3);
	gsl_matrix_memcpy(K,A);
	gsl_linalg_HH_solve(A,b,x);
	
	printf("solution to the following system: \n \n");
	printf("[6.13	-2.90	 5.86] \t [x0] \t [6.23]\n");
	printf("[8.08	-6.31	-3.89] \t [x1] =\t [5.37]\n");
	printf("[-4.36	1.00	 0.19] \t [x2] \t [2.29]\n \n");
	
	
	printf("x = ");
	gsl_vector_fprintf(stdout, x,"%lg");


	// now we found x, and we need to test that it is correct//
	
	gsl_vector* b_result = gsl_vector_alloc(3);

	gsl_blas_dgemv(CblasNoTrans,1.0,K,x,0.0, b_result);
	
	printf("\n\n");	
	printf("testing that b = \n");
	gsl_vector_fprintf(stdout, b, "%lg");

	gsl_matrix_free(A);
	gsl_matrix_free(K);
	gsl_vector_free(x);
	gsl_vector_free(b);
	gsl_vector_free(b_result);


return 0;
}
