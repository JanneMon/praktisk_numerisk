1. In gnuplot, how do you specify the ranges on the x- and y- axes? Hint: in gnuplot: help ranges.


 Syntax:
       [{<dummy-var>=}{{<min>}:{<max>}}]
       [{{<min>}:{<max>}}]

so [xmin:xmax] and [ymin:ymax]


2. In the data-file for gnuplot, how do you separate data intended for separate lines on the plot? Hint: in gnuplot: help data.

Two blank records in a row indicates a break in the dataset

3. Suppose you have a data-file with several columns of data. With gnuplot, how do you plot, say, third column vs. second? Hint: in gnuplot: help plot using.

       plot 'file' using <entry> {:<entry> {:<entry> ...}} {'format'}
 then the following plot commands are all equivalent
       plot 'datafile' using 3:1, '' using 3:2
       plot 'datafile' using (column("Age")):(column(1)), \
                    '' using (column("Age")):(column(2))
       plot 'datafile' using "Age":"Height", '' using "Age":"Weight"


4. Suppose you have a data-file with (x,y) data organised in two columns. How do you plot, say, y² vs. x? Hint: in gnuplot: help plot using.


See 3. 
       plot 'datafile' using 3:1, '' using 3(**2):2



5. How can you find out whether GSL is installed on your box? Hint: gsl-config.

You write gsl-config, an either it knows what you are talking about or it doesn't. I caan also search my repositories pacaur -Ss gsl and it will write [installed] in a blue box if it is installed. 


6. How can you find out which C-flags you need to use with your GSL installation? Hint: gsl-config.

 gsl-config --cflags

7. How can you find out which libraries you need to link with your GSL installation? Hint: gsl-config.

 gsl-config --libs

8. Explain the syntax `command` and $(command) in bash. Hint: Bash command substitution; POSIX shell command substitution.

Apparantly they do the same (substitute command)

9. Why do you need double-dollar, $$(command), in the Makefile? Hint: GNU make: variables in recipes.

$$ Sends to the shell. So if you define a pwd = a string and write @echo 
10. What is "recipe echoing"?

A recipe can be a Makefile, and echoing it is when the recipe is written in the terminal after execution.  

11. What will the following Makefile print? (see fedorov's homepage)

It will print

pwd
/home/janne/Documents/UNI/GR/lecture_7/test
a string
/home/janne/Documents/UNI/GR/lecture_7/test


12. In C-language "type conversion" (or "coersion" or "casting") is allowed with the explicit cast operator (type)variable. So, what does the following line mean?

double  a = *(double*)params;

type casting is a way to convert one variable to another type. So it means that params is assigned double value as double a. 
