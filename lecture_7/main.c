#include<gsl/gsl_sf_hyperg.h>
#include<stdio.h>

int main(){
	for(double x = 10; x<10;x+=0.1)
		printf("%g %g\n",x
				,gsl_sf_hyperg_0F1(0.5,x)
				,gsl_sf_hyperg_0F1(1.0,x)
				,gsl_sf_hyperg_0F1(2.5,x))return 0;
}
