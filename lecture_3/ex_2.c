#include<stdio.h>
#include<math.h>
#include<float.h>
#include<limits.h>

int equal(double,double,double,double);
int main(){
	//i//
	
	int i = 1;
	while(i+1>i){i++;}
	printf("For while loop: My max int = %i\n",i);
	
	for(int i = 1; i+1>i;){i++;}
	printf("For for loop: My max int = %i\n",i);
	
	int j = 1;
	do{j++;}
	while(j+1>j);
	printf("For do while: My max int = %i\n",j);

	//ii//
	
	int k = 1;
	while(k-1<k){k--;}
	printf("For while loop: My min int =  %i\n",k);

	for(int k = 1; i-1<k;){k--;}
	printf("For for loop: My min int = %i\n",k);

	int h = 1;
	do{h--;}
	while(h-1<h);
	printf("For do while: My min int = %i\n",h); 

	//iii//
	
	double xd = 1; long double lx = 1; float fx;
	
	while(1+xd!=1){xd/=2;}
	xd*=2;
	while(1+lx!=1){lx/=2;}
	lx*=2;
	while(1+fx!=1){fx/=2;}
	fx*=2;

	printf("For while loops:\n\n");
	printf("DBL_EPSILON = %g\n Double = %g\n",DBL_EPSILON,xd);
	printf("LDBL_EPSILON = %Lg\n Long double = %Lg\n",LDBL_EPSILON,lx);
	printf("FLT_EPSILON = %f\n Float = %f\n\n",FLT_EPSILON,fx);

	for(double xd = 1; 1+xd!=1;){xd/=2;}
	xd*=2;
	for(long double lx = 1; 1+lx!=1;){lx/=2;}
	lx*=2;
	for(float fx = 1; 1+fx!=1;){fx/=2;}
	fx*=2;

	printf("For for loops:\n\n");
	printf("DBL_EPSILON = %g\n Double = %g\n",DBL_EPSILON,xd);
	printf("LDBL_EPSILON = %Lg\n Long Double %Lg\n", LDBL_EPSILON,lx);
	printf("FLT_EPSILON = %f\n FLoat = %f\n\n",FLT_EPSILON,fx);
	
	double xd2 = 1; long double lx2 = 1; float fx2;

	do{xd2/=2;}
	while(1+xd2!=1);
	xd2*=2;
	
	do{lx2/=2;}
	while(1+lx2!=1);
	lx2*=2;
	
	do{fx2/=2;}
	while(1+fx2!=1);
	fx2*=2;

	printf("For do while loops:\n\n");
	printf("DBL_EPSILON = %g\n Double = %g\n", DBL_EPSILON,xd2);
	printf("LDBL_EPSILON = %Lg\n Long double = %Lg\n",LDBL_EPSILON,lx2);
	printf("FLT_EPSILON = %f\n Float = %f\n",FLT_EPSILON,fx2);

	//2i//
	
	int max = INT_MAX/3;
	float sum_up_float = 0; 
	float sum_down_float = 0;
	
	for(int k = 1; k<max;k++){sum_up_float+=1.0/k;}
	printf("sum_up_float = %f\n",sum_up_float);
	
	for(int k = 1; k<max; k++){sum_down_float+=1.0/(max-k);}
	printf("sum_down_float = %f\n",sum_down_float);
	
	double sum_up_double = 0;
	double sum_down_double = 0;
       	for(int u = 1; u<max; u++){sum_up_double+=1.0/u;}
	printf("sum_up_double = %lg\n",sum_up_double);
	
	for(int u = 1; u<max; u++){sum_down_double+=1.0/u;}
	printf("sum_down_double = %lg\n", sum_down_double); 
	
	//3i//

	int f = equal(2,1,2,1);
	printf("condition = %i\n",f);	
return 0;

}
