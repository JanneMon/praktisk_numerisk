#include<stdio.h>
#include<float.h>
#include<math.h>
#include<limits.h>

int equal(double a, double b, double tau, double epsilon){
	int condition;

	double diff = fabs(a-b);
	double diff2 = diff/(fabs(a)+fabs(b));

	if(diff < tau || diff2 < epsilon/2.0){condition = 1;}
       	else{condition = 0;}	

return condition;
}	
