#include<stdio.h>
#include<stdlib.h>
int main(int argc, char** argv){
	int n=0;
	if(argc>1)n=atoi(argv[1]);
	double x[n];

	for(int i=0;i<n;i++)
		scanf("%*s %*s %lg %*[^\n]s",&(x[i]));

	for(int i=0;i<n;i++)
		printf("x[%i]=%lg\n",i,x[i]);

return 0;
}
