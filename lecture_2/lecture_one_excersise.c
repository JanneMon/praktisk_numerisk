#include <stdio.h>
#include <tgmath.h>
#define PI 3.1415926535
#define e  2.7182818284590452354
int main(){
	double gam = tgamma(5.0);
	double bes = j1(0.5);
	complex double c1 = csqrt(-2);
	complex double c2 = exp(I);
	complex double c3 = exp(PI*I);
	complex double c4 = cpow(I,e);

	printf("gamma = %lf\n",gam);
	printf("bessel = %lf\n",bes);
	printf("sqrt(-2.0) = %lf + i%lf \n",creal(c1),cimag(c1));
	printf("Exp(I) = %lf + i%lf \n", creal(c2),cimag(c2));
	printf("Exp(I*PI) = %lf + i%lf \n",creal(c3),cimag(c3));
	printf("I^exp = %lf + i%lf \n", creal(c4),cimag(c4));
	
	//PART TWO//
	
	float flt        = 0.11111111111111111111111111111;
	double dbl       = 0.11111111111111111111111111111;
	long double ldbl = 0.111111111111111111111111111111L;

	printf("Float = %.25g \n",flt);
	printf("Double  = %.25lg \n",dbl);
	printf("Long double = %0.25Lg \n",ldbl); //25 for 25 ciffers//


	return 0;
}
