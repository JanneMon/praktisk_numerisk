#include<stdio.h>
#include<math.h>
#include<gsl/gsl_integration.h>


double int1(double x, void* params){
	double alfa = *(double*) params; 
	double function1 = exp((-alfa*x*x)/2.0);  
	return function1;
}
double int2(double x, void* params){
	double alfa = *(double*) params;
	double function2 = exp(-alfa*x*x)*(-1/2*alfa*alfa*x*x + alfa/2 + (x*x)/2);
	return function2;
}
double myint(double alfa, double* norm, double* sandwich){
	gsl_function sys1;
	gsl_function sys2;
	sys1.function = int1;
	sys2.function = int2;
	sys1.params=(void*) &alfa;
	sys2.params=(void*) &alfa;
	
	double res1, err1,res2,err2, acc=1e-6,eps=1e-6;
	size_t limit = 100;
	
	gsl_integration_workspace* workspace =
	gsl_integration_workspace_alloc(limit);	
	
	gsl_integration_qagi(&sys1, acc, eps, limit, workspace, &res1, &err1);
	gsl_integration_qagi(&sys2, acc, eps, limit, workspace, &res2, &err2);
	
	(*norm)     = res1;
	(*sandwich) = res2;

	gsl_integration_workspace_free(workspace);

}

int main(){
	double norm, sandwich;
	for(double alfa=0.01;alfa<=2*M_PI+1;alfa+=0.01){
		myint(alfa, &norm, &sandwich); 
		printf(" %lg\t %lg\t %lg\t %lg\t \n",alfa, norm, sandwich, sandwich/norm);
	}
	return 0;
}
