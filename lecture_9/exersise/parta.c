#include<stdio.h>
#include<math.h>
#include<gsl/gsl_integration.h>


double f(double x, void* params){
	double F = log(x)/sqrt(x);
	return F;
}

int main(){
	gsl_function func;
	func.function= &f;
	func.params=NULL;
	
	double res;
	double err;
	double acc=1e-6;
	double eps=1e-6;
	size_t limit = 100;
	int boundaries[2] = {0,1};
	
	gsl_integration_workspace* workspace =
		gsl_integration_workspace_alloc(limit);	

	gsl_integration_qags(&func,boundaries[0], boundaries[1],acc, eps, limit, workspace, &res, &err);

	printf("The integral of the function ln(x)/sqrt(x) from 0 to 1 is %g\n",res);
		
	gsl_integration_workspace_free(workspace);

	return 0;
}
