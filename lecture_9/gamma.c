#include<gsl/gsl_integration.h>
#include<math.h>

double gamma_integrand(double x, void* params){
	double z =* (double *)params;//interpret params as if it points to a double//
	return pow(x,z-1)*exp(-x);
}

double mygamma(double z){
	gsl_function f;
	f.function = &gamma_integrand;
	f.params = (void*)&z;

	size_t limit = 10;
	double acc = 1e-6, eps = 1e-6, result, err;
	gsl_integration_workspace* workspace =
		gsl_integration_workspace_alloc(limit);
	gsl_integration_qagiu(&f,0, acc,eps,limit, workspace, &result,&err); 
	
	gsl_integration_workspace_free(workspace);

	return result;
}
