#include<math.h>
#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>
#include<assert.h>

int function(double r,const double y[],double f[],void* params){
	double epsilon = *(double*) params;
	
	f[0]=y[1];
	f[1]=-2*y[0]*(epsilon+(1/r));
return GSL_SUCCESS;
}

double odeiv(double r, double epsilon){
	assert(r>=0);
	const double rmin = 1e-3;
	if(r<rmin) return r-r*r;


	gsl_odeiv2_system diffsolve;
	diffsolve.function=function;
	diffsolve.dimension=2;
	diffsolve.params=(void*)&epsilon;

	double hstart=copysign(0.1,r);
	double acc=1e-6;
	double eps=1e-6;

	gsl_odeiv2_driver* driver = gsl_odeiv2_driver_alloc_y_new
			(&diffsolve,gsl_odeiv2_step_rkf45,hstart,acc,eps);	

	double r_low=1e-3;
	double y[2]={r_low-r_low*r_low,1-2*r_low};//lower condition for f and f' where f-> r-r²
	
	gsl_odeiv2_driver_apply(driver,&r_low,r,y);

	gsl_odeiv2_driver_free(driver);
	return y[0];
}

/*int main(){
	for(double x=0;x<=3.;x+=0.1)
		printf("%g\t %g\t %g\n",x,mysolve(x),1/(1+exp(-x)));

	printf("\n");
	printf("\n");
return 0;
}*/
