#include<stdio.h>
#include<math.h>
#include<gsl/gsl_multiroots.h>
#include<gsl/gsl_vector.h>
#include<assert.h>

int deffunc(const gsl_vector* current_point, void* params, gsl_vector* f){
	double x = gsl_vector_get(current_point,0);
	double y = gsl_vector_get(current_point,1);
	
	double dfdx = 400*(x*x*x)-x*(400*y*x)+2*x-2;
	double dfdy = 200*(y-x*x);	

	gsl_vector_set(f,0,dfdx);
	gsl_vector_set(f,1,dfdy);

	return GSL_SUCCESS;	
}

int rosenbrock(double* xmin, double* ymin, int* iteration){
	//defining dimensions//	
	int dimensions = 2;
	
	//defining fsolver type, and workspace allocation//
	const gsl_multiroot_fsolver_type* type = gsl_multiroot_fsolver_hybrids;
	gsl_multiroot_fsolver* solver = gsl_multiroot_fsolver_alloc(type,dimensions);

	//defining function to solve//
	gsl_multiroot_function func;
	func.f = deffunc;
	func.n = dimensions;
	func.params = NULL;
	
	//initial guess into an array of length 2//
	double guess[2] = {0,3};

	//allocate space for initial guess vector and set entries//
	gsl_vector* g = gsl_vector_alloc(dimensions);
	gsl_vector_set(g, 0, guess[0]);
	gsl_vector_set(g, 1, guess[1]);
	
	//setting or resetting the fsolver
	gsl_multiroot_fsolver_set(solver,&func,g);
	

	//defining parameters for iteration
	int result, i=0, ulimit = 1000;
	double epsilon = 1e-7;

	// the function stars searching for a minimum//
	do{ 
		i++;
		result = gsl_multiroot_fsolver_iterate(solver);
			//break if the iteration gets stuck (see example in documentation//
			if(result) break;
//	gsl_vector_set(f,0, pow(1-x,2) + 100*pow(y-x*x,2));
		result = gsl_multiroot_test_residual(solver->f,epsilon);

	//loop until result gives a false value, i.e. if the solver comes under the minimum accepted value//
	}while(result == GSL_CONTINUE && i < ulimit);
	
	*xmin = gsl_vector_get(solver->x,0);
	*ymin = gsl_vector_get(solver->x,1);
	*iteration = i;
	
/*
	//printout error//
	printf("\n Operational status = %s \t @ %i iterations\n",gsl_strerror (result),i);

	//printout of x and y values for minimum found
	printf("\nMinimum value for the Rosenbrock function found at: \n\n");
	printf("x = \t %g \t and y = \t %g \n",gsl_vector_get(solver->x,0),gsl_vector_get(solver->x,1));
*/	
	//free the allocated memory
	gsl_vector_free(g);
	gsl_multiroot_fsolver_free(solver);
	return 0;
}

/*int main(){
	//double minfinder(double);
	for(double x = 0; x<17;x+=0.1)
		printf("%lg\t %lg\n",x,minfinder(x));
	
	return 0;
}*/
