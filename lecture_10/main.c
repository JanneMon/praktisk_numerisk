#include<gsl/gsl_vector.h>
#include<gsl/gsl_multiroots.h>
#include<assert.h>
#define FSOLVER gsl_multiroot_fsolver_broyden


double rosenbrock();
double odeiv(double r, double epsilon);

int auxiliary(const gsl_vector *x, void *params, gsl_vector *f){
	double epsilon = gsl_vector_get(x,0);
	assert(epsilon<=0);
	double rmax = *(double*)params;
	double fval = odeiv(rmax, epsilon);
	gsl_vector_set(f,0,fval);
	
	return GSL_SUCCESS;
}
int main(){
	//ROSENBROCK//
	double xmin, ymin;
	int iteration;
	rosenbrock(&xmin,&ymin,&iteration);
	//printout error//
	printf("# The minimmum of the Rosenbrock function is at (x,y) =(%g,%g)\n", xmin,ymin);
	printf("The number of iterations are %i \n",iteration);

	//HYDROGEN//
	
	double rmax = 8;
	int dimension = 1;
	
	gsl_multiroot_fsolver* solver = 
		gsl_multiroot_fsolver_alloc(FSOLVER, dimension);
	
	gsl_multiroot_function F;
	F.f = auxiliary;
	F.n = dimension;
	F.params = (void*)&rmax;

	gsl_vector *start = gsl_vector_alloc(dimension);
	gsl_vector_set(start,0,-1);

	gsl_multiroot_fsolver_set(solver, &F, start);

	int result, it = 0;
	const double abs = 1e-3;

	do{
		it++;
		result = gsl_multiroot_fsolver_iterate(solver);
		if(result) break;
		result = gsl_multiroot_test_residual(solver->f, abs);
		if(result == GSL_SUCCESS) fprintf(stderr,"converged\n");
		
		fprintf(stderr,"iter = %i\n",it);
		fprintf(stderr,"epsilon %g\n",gsl_vector_get(solver->x,0));
		fprintf(stderr,"f(rmax) = %g\n",gsl_vector_get(solver->f,0));
		fprintf(stderr,"\n");
	}while(result == GSL_CONTINUE && it<100);

	double epsilon = gsl_vector_get(solver->x,0);
	printf("# rmax, epsilon\n");
	printf("%g \t %g \n",rmax, epsilon);
	printf("\n\n");

	printf("# r, odeiv(r, epsilon), exact \n");
	
	for(double r=0; r<=rmax; r+=0.2){
		printf("%g \t %g \t %g\n",r,odeiv(r,epsilon),r*exp(-r));}
	
	gsl_multiroot_fsolver_free(solver);
	gsl_vector_free(start);	
}
