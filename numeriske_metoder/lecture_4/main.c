#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<math.h>
#include<gsl/gsl_blas.h>
#include<stdlib.h>

void print_matrix(const char* s, gsl_matrix* A );
void matrix_decomp(gsl_matrix* A, gsl_matrix* Q, gsl_matrix* R );
void inverse(const gsl_matrix* Q, const gsl_matrix* R, gsl_matrix* B);
void print_vector(const char* s, gsl_vector* b);
void solve(const gsl_matrix* Q, const gsl_matrix* R, gsl_vector*  b, gsl_vector* x);

double funcs(int n, double xi){
	switch(n){
	case 0: return log(xi); break;
	case 1: return 1.0;    break;
	case 2: return xi;      break;
	default: {fprintf(stderr, "funcs: wrong n: %d", n); return NAN; 
		}
	}
}

int main(int argc, const char * argv[]){
	int n = 9;
	int m = 3;

	gsl_matrix* A     = gsl_matrix_alloc(n,m);
	gsl_matrix* Acopy = gsl_matrix_alloc(n,m);
	gsl_matrix* R     = gsl_matrix_alloc(m,m);
	gsl_vector* b     = gsl_vector_alloc(n);
	gsl_vector* c     = gsl_vector_alloc(m);
//	gsl_vector* x     = gsl_vector_alloc(m);
	gsl_matrix* Q     = gsl_matrix_alloc(n,m);
	gsl_matrix* Ri    = gsl_matrix_calloc(m,m);
	gsl_matrix* RRT   = gsl_matrix_alloc(m,m);
	gsl_matrix* ID    = gsl_matrix_alloc(m,m);

	gsl_matrix_set_identity(ID);
	double x[9]       = {0.1,1.33,2.55,3.78,5.0,6.22,7.45,8.68,9.9};
	double y[9]       = {-15.3, 0.32,2.45,2.75,2.27,1.35,0.157,-1.23,-2.75};
	double dy[9]      = {1.04, 0.594, 0.983, 0.998, 1.11, 0.398, 0.535, 0.968, 0.478};
	
	//prints the initial values to be fitted//
	
	/*fprintf(stderr, "Values to be fitted:");
	fprintf(stderr,"\n\n");
	fprintf(stderr,"x[i] \t y[i] \t dy[i] \t\n");*/
	for(int i = 0;i < n; i++){
		printf("%g \t %g \t %g\n", x[i], y[i], dy[i]);
	}
	
	//sets a 9*3 matrix consisting of 3 coloumns with the three different functions described in funcs, and the corresponding values for a given x[i]. Aij is given by equation (7) in the chapter.
	for(int i = 0; i < n; i++){
		for(int j = 0; j < m; j++){
			gsl_matrix_set(A,i,j,funcs(j,x[i])/dy[i]);
		}
	}


	gsl_matrix_memcpy(Acopy,A);

	for(int i = 0; i < n; i++){
		gsl_vector_set(b,i,y[i]/dy[i]);  
	}

	//c = R^(−1)QTb is the solution to the least squares fit.	
	print_matrix("Matrix A",A);
	print_vector("Vector b",b);

	matrix_decomp(A,Q,R);
	print_matrix("Matrix Q",Q);
	print_matrix("Matrix R",R);
	solve(Q,R,b,c);
	
	// Set Inverse matrix
	inverse(ID,R,Ri);
	print_matrix("Matrix Ri",Ri);
	
	// Finding errors by RTR^-1
	gsl_blas_dgemm(CblasNoTrans,CblasTrans,1,Ri,Ri,0,RRT);
	print_matrix("Matrix RRT",RRT);
	
	//Errors (diagonal in RRT). Calculated using eq. 14 and 15 in chapter. 
	double s1 = gsl_matrix_get(RRT, 0, 0);
	double s2 = gsl_matrix_get(RRT, 1, 1);
	double s3 = gsl_matrix_get(RRT, 2, 2);
	
	//Coefficients 
	double c1 = gsl_vector_get(c,0);
	double c2 = gsl_vector_get(c,1);
	double c3 = gsl_vector_get(c,2);
	

	fprintf(stderr,"\n\n The calculated fit is (%g +- %g)*log(x)+(%g+-%g)+(%g+-%g)*x\n",
        c1, pow(s1,0.5),
        c2, pow(s2,0.5),
        c3, pow(s3,0.5));

	double X, Y, Ymin1, Ymin2, Ymin3, Ymax1, Ymax2, Ymax3;
	printf("\n\n");
	for (int i = 0; i < 1000; i++) {
		X = i/100.0;
		Y = gsl_vector_get(c,0)*log(X) + gsl_vector_get(c,1) + gsl_vector_get(c,2)*X;

    		Ymin1 = (c1-pow(s1,0.5))*log(X)+c2+c3*X;
           	Ymin2 =	(c1*log(X))+(c2-pow(s2,0.5)) +c3*X;
            	Ymin3 =	(c1*log(X))+c2+(c3-pow(s3,0.5))*X;

   		Ymax1 = (c1+pow(s1,0.5))*log(X) + c2 + c3*X;
        	Ymax2 = (c1*log(X)) + (c2+pow(s2,0.5)) + c3*X;
            	Ymax3 =	(c1*log(X))+ c2 + (c3+pow(s3,0.5))*X;
   				
	       		printf("%g %g %g %g %g %g %g %g \n",X,Y,Ymin1,Ymin2,Ymin3,Ymax1,Ymax2,Ymax3);
		}

	// Free the prisoners!
	gsl_matrix_free(A);
	gsl_matrix_free(R);
	gsl_matrix_free(Q);
	gsl_matrix_free(Ri);
	gsl_vector_free(c);
	gsl_matrix_free(RRT);
	gsl_matrix_free(Acopy);
		
}
