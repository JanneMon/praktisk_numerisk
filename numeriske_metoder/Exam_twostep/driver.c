#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_blas.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<math.h>
#include<assert.h>

void rkstep12(
	double t,				
	double h,
	gsl_vector* ynow, 		
	void f(double t, gsl_vector* y, gsl_vector* dydt),
	gsl_vector* ynew,								gsl_vector* err
);

void rkstep23( double t,
	       double h, 
	       gsl_vector* ynow,
	       void f(double x, gsl_vector* y, gsl_vector* dydt), 
	       gsl_vector* ynew,
	       gsl_vector* err);
int driver(
		double *t,                      //target value
		double b,			//end point of integration
		double* h,			//step size
		gsl_vector* ynow, 		//current y(t)
		double acc,			//absolute accuracy
		double eps, 			//relative accuracy
		void stepfunc(
			double tprev,double t, double h, gsl_vector* yprev, gsl_vector* ynow, void f(double t, gsl_vector* y,gsl_vector*dydt),gsl_vector* ynew, gsl_vector* err),
		void f(double t, gsl_vector* y, gsl_vector* dydt),
		gsl_vector* err
){
	int iter = 0, n = ynow->size;
	double tol, tprev;
	
	gsl_vector* yprev = gsl_vector_alloc(n);
	gsl_vector* ynew  = gsl_vector_alloc(n);
	//gsl_vector* err = gsl_vector_alloc(n);
	
 

       //Generate first value using Runga-Kutta12 outside loop
	if (fabs(*t + *h) > fabs(b)) *h = b - *t;
	rkstep12(*t,*h, ynow,f,ynew,err);
	tol = (acc+eps*gsl_blas_dnrm2(ynew));
	if(tol > gsl_blas_dnrm2(err)){
			
		tprev=*t;
		*t += *h;
		gsl_vector_memcpy(yprev, ynow);
		gsl_vector_memcpy(ynow, ynew);
		
		iter++;
	}
	*h = *h*pow(tol/gsl_blas_dnrm2(err),0.25)*0.95;
//	fprintf(stderr,"h: %g\n",*h);
//	fprintf(stderr,"tprev: %g\n",tprev);

	//Iterate by two step method
	do{
		if(fabs(*t + *h) > fabs(b)) *h = b - *t;
		if(*t >=b) break;
		stepfunc(tprev,*t, *h,yprev,ynow,f,ynew, err);

		tol = (acc+eps*gsl_blas_dnrm2(ynew));
		//fprintf(stderr,"First if works\n");
		if(tol > gsl_blas_dnrm2(err)){
			tprev = *t;
			*t += *h;
			gsl_vector_memcpy(yprev, ynow);
			gsl_vector_memcpy(ynow,ynew);
			iter++;
		}
		*h = *h*pow(tol/gsl_blas_dnrm2(err), 0.25)*0.95;
	}while(iter < 1e6);
/*
	gsl_vector_free(yprev); gsl_vector_free(err); gsl_vector_free(ynew);
	*/
	return iter;
}

int driver_path(
	gsl_matrix* path,
	double b,
	double* h,
	double acc,
	double eps,
	void stepfunc(
		double tprev, double t, double h, gsl_vector* yprev,gsl_vector* ynow,
		void f(double t, gsl_vector* y, gsl_vector* dydt),
		gsl_vector* ynew, gsl_vector* err
		),
	void f(double t, gsl_vector* y, gsl_vector* dydt)
){
	int n = path->size2-1, max_steps = path->size1, steps = 0;
	double tol, t, tprev;
	
	gsl_vector* yprev = gsl_vector_alloc(n);
	gsl_vector* ynew  = gsl_vector_alloc(n);
	gsl_vector* ynow  = gsl_vector_alloc(n);;
	gsl_vector* err   = gsl_vector_alloc(n); 

	//Generate first step with runge-kutta method (since two-step cannot start on its own)	
	t = gsl_matrix_get(path,steps,0);
	for(int i = 0; i<n; i++){
		gsl_vector_set(ynow,i,gsl_matrix_get(path,steps,i+1));
	}
	if(fabs(t + *h) > fabs(b)) *h = b-t;
	
	rkstep12(t,*h,ynow,f,ynew,err);
	tol = (acc+eps*gsl_blas_dnrm2(ynew));	
	
	if(tol > gsl_blas_dnrm2(err)){
		steps++;	
		gsl_matrix_set(path,steps,0,t+ *h);
		
		for(int i = 0; i<n; i++){
			gsl_matrix_set(path,steps, i+1,gsl_vector_get(ynew,i));
		}
	}

	*h = *h*pow(tol/gsl_blas_dnrm2(err),0.25)*0.95;

	while(gsl_matrix_get(path,steps,0) < b){
		
		tprev = gsl_matrix_get(path, steps-1,0);
		t     = gsl_matrix_get(path, steps,0);

		for(int i = 0; i<n; i++){
			gsl_vector_set(yprev,i,gsl_matrix_get(path,steps-1,i+1));
			gsl_vector_set(ynow,i,gsl_matrix_get(path,steps,i+1));
		}

		if(fabs(t + *h) > fabs(b)) *h = b - t;
		if(t>=b) break;

		stepfunc(tprev,t,*h,yprev,ynow,f,ynew,err);
		tol = (acc + eps*gsl_blas_dnrm2(ynew));

		if(tol > gsl_blas_dnrm2(err)){
			steps++;
			if(steps+1 > max_steps){
				fprintf(stderr, "\n Maximmum number of steps have been reached\n");
				break;
			}

			gsl_matrix_set(path,steps,0, t+*h);
			for(int i = 0; i<n; i++){
				gsl_matrix_set(path, steps, i+1, gsl_vector_get(ynew,i));
			}
		}
		if(err > 0){
			*h = *h*pow(tol/gsl_blas_dnrm2(err), 0.25)*0.95;
		}
		else *h = 2;
	}
	gsl_vector_free(ynew); 
	gsl_vector_free(err);
	gsl_vector_free(ynow);
	gsl_vector_free(yprev);

	return steps;
	
}

/*void rkstep23( double t,
	       double h, 
	       gsl_vector* ynow,
	       void f(double x, gsl_vector* y, gsl_vector* dydt), 
	       gsl_vector* ynew,
	       gsl_vector* err){
	

	// set initial values and allocate vectors:
	int i;
	int n = ynow->size;
	gsl_vector* k1 = gsl_vector_calloc(n);
	gsl_vector* k2 = gsl_vector_calloc(n);
	gsl_vector* k3 = gsl_vector_calloc(n);
	gsl_vector* k4 = gsl_vector_calloc(n);
	gsl_vector* yt = gsl_vector_calloc(n);

	// call function to get values for k1 and set yt:
	f(t, ynow, k1); //function call with current y
	for(i=0; i<n; i++){
		gsl_vector_set(yt, i, gsl_vector_get(ynow, i) + 1.0/2 * gsl_vector_get(k1, i) * h);
		}

	// now call function to get values for k2 and set yt:
	f(t+1.0/2*h, yt, k2); // function  call with yt and k2
	for(i=0; i<n; i++){
		gsl_vector_set(yt, i, gsl_vector_get(ynow, i) + 3.0/4 * gsl_vector_get(k2, i) * h);
		}

	// now call function to get values for k3 and set yt:
	f(t+3.0/4*h, yt, k3); //function call with yt and k3
	for(i=0; i<n; i++){
		gsl_vector_set(ynew, i,  gsl_vector_get(ynow, i) + (2.0/9 * gsl_vector_get(k1, i)
			 	 + 1.0/3 * gsl_vector_get(k2, i)+ 4.0/9 * gsl_vector_get(k3, i))* h);
		}

	// now call function to get values for k4 and set yt and dy:
	f(t+h, ynew, k4); //function call with ynew and k4
	for(i=0; i<n; i++){
		gsl_vector_set(ynew, i,  gsl_vector_get(ynow, i) + (7.0/24 * gsl_vector_get(k1, i) + 1.0/4 * gsl_vector_get(k2, i)
			+ 1.0/3 * gsl_vector_get(k3, i) + 1.0/8 * gsl_vector_get(k4, i))* h);
		gsl_vector_set(err, i, gsl_vector_get(ynew, i) - gsl_vector_get(yt, i));
		}

	// freeeedom
	gsl_vector_free(k1);
	gsl_vector_free(k2);
	gsl_vector_free(k3);
	gsl_vector_free(k4);
	gsl_vector_free(yt);

}
*/
void rkstep12(
	double t,				
	double h,
	gsl_vector* ynow, 		
	void f(double t, gsl_vector* y, gsl_vector* dydt),
	gsl_vector* ynew,								gsl_vector* err 							
){

	int n = ynow->size;

	gsl_vector* k0 = gsl_vector_alloc(n);
	gsl_vector* k12 = gsl_vector_alloc(n);

	gsl_vector_memcpy(ynew,ynow);

	f(t,ynow,k0);

	gsl_blas_daxpy(h/2, k0,  ynow);	
	f(t+h/2, ynow, k12);
	gsl_blas_daxpy(h, k12, ynew);	

	gsl_vector_sub(k0,k12);
	gsl_vector_scale(k0,h/2);
	gsl_vector_memcpy(err,k0);
	gsl_vector_free(k0); gsl_vector_free(k12);
}
