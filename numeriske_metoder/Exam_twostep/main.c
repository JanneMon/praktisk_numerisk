#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_blas.h>
#include<assert.h>
#include<tgmath.h>

/*	Harmonic osc.. u''=-u -> y1=u, y2=u' -> y1'=y2, y2'=-y1	*/
void harmonic(double x, gsl_vector * y, gsl_vector * dydx){
	gsl_vector_set(dydx,0,gsl_vector_get(y,1));
	gsl_vector_set(dydx,1,-gsl_vector_get(y,0));
}

void two_step(
	double tprev,									
	double t,
	double h,										 gsl_vector* yprev,
	gsl_vector* ynow, 								
	void f(double t, gsl_vector* y, gsl_vector* dydt),
	gsl_vector* ynew,
	gsl_vector* err 								
);

void two_step_extra(
	double tprev,
	double t,
	double h,
	gsl_vector* yprev,							
	gsl_vector* ynow, 							
	void f(double t, gsl_vector* y, gsl_vector* dydt),
	gsl_vector* ynew,							
	gsl_vector* err 								
);

void rkstep12(
	double t,							
	double h,
	gsl_vector* ynow, 							
	void f(double t, gsl_vector* y, gsl_vector* dydt),
	gsl_vector* ynew,
	gsl_vector* err 								
);

int driver(
	double* t,                            
	double b,
	double* h,                             
	gsl_vector* ynow, 
       	double acc,	
	double eps,                           
	void stepfunc(                        
		double tprev, double t, double h,
		gsl_vector* yprev, gsl_vector *ynow,
		void f(double t,gsl_vector *y,gsl_vector *dydt),
		gsl_vector*ynew, gsl_vector*err
		),
	void f(double t,gsl_vector *y,gsl_vector* dydt),
	gsl_vector* err
);

int driver_path(
	gsl_matrix* path,
	double b,                            
	double* h,                             
	double acc,                           
	double eps,                       
	void stepfunc(                          
		double tprev, double t, double h,
		gsl_vector *yprev, gsl_vector *ynow,
		void f(double t,gsl_vector *y,gsl_vector *dydt),
		gsl_vector *ynew, gsl_vector*err
		),
	void f(double t, gsl_vector* y,gsl_vector *dydt)
);


int main(){
	fprintf(stderr,"\n_____________EXAMINATION: Practical programming and Numerical methods_________________________\n");
	fprintf(stderr,"Janne Højmark Mønster 201405523\n");
	fprintf(stderr,"Date: 15/6-18\n\n");
	fprintf(stderr,"==============================ODE: A TWO STEP FUNCTION==========================================\n");

	fprintf(stderr,"The method is apllied both with and without extra evaluation. First step is made with traditional Runge-Kutta with rkstep12 stepper(see lecture 7 ODE solution)\n");
	fprintf(stderr,"We wish to solve the harmonic oscillator:\n");
	fprintf(stderr," y'' = -y, y(0) = 1 and y'(0) = 0\n");

	
	//Defining limitvalues and accuracy parameters:
	int n = 2;
	double a = 0, b = M_PI/2, h, acc = 1e-6, eps = 1e-4;
	gsl_vector* y   = gsl_vector_alloc(n);
	gsl_vector* err = gsl_vector_alloc(n);

	fprintf(stderr,"Using accuracy = %g, and relative accuracy %g\n\n",acc,eps);
//	=========================== TWO STEP =============================

	//Setting y:
	gsl_vector_set(y,0,1);
	gsl_vector_set(y,1,0);
	h = copysign(0.01,b-a);

	//applying driver:
	int iter = driver(&a,b,&h,y,acc,eps, &two_step, &harmonic,err);
	
	fprintf(stderr,"Simple two step method\n\n");
	fprintf(stderr,"Testfunction\tSolved function\tExact solution\tCalculated error\tExact error\n");
	fprintf(stderr,"y' cos(PI/2)\t%4g\t%1f\t%4g\t%4g\n",gsl_vector_get(y,0),cos(a),gsl_vector_get(err,0),gsl_vector_get(y,0)-cos(a));
	fprintf(stderr,"y -sin(PI/2)\t%4g\t%1f\t%4g\t%4g\n",gsl_vector_get(y,1),-sin(a),gsl_vector_get(err,1),gsl_vector_get(y,1)+sin(a));
	fprintf(stderr,"Number of iterations: %5i\n\n",iter);


//	================== TWO STEP WITH EXTRA EVALUATION =====================

	//Reset start parameters
	a = 0;
	gsl_vector_set(y,0,1);
	gsl_vector_set(y,1,0);
	h = copysign(0.01,b-a);

	iter = driver(&a,b,&h,y,acc,eps,&two_step_extra,&harmonic,err);
	
	fprintf(stderr,"Two step method with extra evaluation\n\n");
	fprintf(stderr,"Testfunction\tSolved function\tExact solution\tCalculated error\tExact error\n");
	fprintf(stderr,"y' cos(PI/2)\t%g\t%1f\t%4g\t%4g\n",gsl_vector_get(y,0),cos(a),gsl_vector_get(err,0),gsl_vector_get(y,0)-cos(a));
	fprintf(stderr,"y -sin(PI/2)\t%4g\t%1f\t%4g\t%4g\n",gsl_vector_get(y,1),-sin(a),gsl_vector_get(err,1),gsl_vector_get(y,1)+sin(a));
	fprintf(stderr,"Number of iterations: %5i\n\n",iter);

//	========================== STORING THE PATH ==========================
	
	fprintf(stderr,"Modified version of ODE driver stores the path. Here we use the function describing the orbit, which can be seen in plot.svg\n");
	
	int max = 1e6;
	n = 2;
	a = 0;
	b = 2*M_PI;
	acc = 1e-4;
	eps = 1e-4;

	gsl_matrix *path = gsl_matrix_calloc(max,n+1);

	gsl_matrix_set(path,0,0,a);
	gsl_matrix_set(path,0,1,1);
	gsl_matrix_set(path,0,2,-0.5);
	h = copysign(0.01,b-a);

	int steps = driver_path(path,b,&h,acc,eps,&two_step_extra,harmonic);

	for(int j = 0; j<steps; j++){
		fprintf(stdout,"%g\t%g\t%g\n",gsl_matrix_get(path,j,0),gsl_matrix_get(path,j,1),gsl_matrix_get(path,j,2));
	}


	// Free allocated memory
	gsl_vector_free(y);
	gsl_vector_free(err);
	gsl_matrix_free(path);
	
	
	return EXIT_SUCCESS;
}
