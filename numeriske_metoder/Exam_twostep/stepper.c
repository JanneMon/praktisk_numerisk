#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <math.h>

void two_step(
	double tprev,
	double t,
	double h,
	gsl_vector* yprev,
	gsl_vector* ynow,
		void f(double t, gsl_vector* y, gsl_vector* dydt),
		gsl_vector* ynew,
		gsl_vector* err
){
	int n = ynow->size;
	gsl_vector* ydiff = gsl_vector_alloc(n);

	f(t,ynow,ydiff);	// y'i = f(xi,yi)

	
	//Find coefficient c//
	//c = (yi -1 - yi + y'(xi-xi-i))/(xi-xi-1)² (28)
	// yi = ynow
	//yprev = yi-1
	//t = xi
	//tprev = xi-1
	//yf = y'

	gsl_vector_sub(yprev,ynow);                       //yi-1-yi -> yi-1
	gsl_vector_scale(ydiff,(t-tprev));               //ydiff * (x-xi-1) -> ydiff
	gsl_vector_add(yprev,ydiff);     	        //yi-1 - yi + y'(xi-xi-1)->yi-1
	gsl_vector_scale(yprev,pow((t-tprev),-2));       //yprev = c
	
	//Errors; dy = ch²
	gsl_vector_memcpy(err,yprev);
	gsl_vector_scale(err,pow(h,2));

	//Second order:
	f(t,ynow,ydiff);                         //reset yi'
	gsl_vector_scale(ydiff,h);              //y'*(t+h-t) = y'*h->y' (by inserting xi+1 = xi +h in 27)
	gsl_vector_scale(yprev,pow(h,2));      //c*(t+h-t)² -> c(yprev)
	gsl_vector_add(yprev,ydiff);             //y'*h +c*h² -> c(ypev)
	gsl_vector_add(yprev,ynow);          //yi + y'h + ch² -> c(yprev)
	gsl_vector_memcpy(ynew,yprev);      //yprev (c) to ynew

	gsl_vector_free(ydiff);
}

void two_step_extra(
		double tprev,
		double t,
		double h,
		gsl_vector* yprev,
		gsl_vector* ynow,
		void f(double t, gsl_vector* y, gsl_vector* dydt),
		gsl_vector *ynew,
		gsl_vector *err
){
	int n = ynow->size;
	gsl_vector* ydiff = gsl_vector_alloc(n);
	gsl_vector* y  = gsl_vector_alloc(n);
	
	gsl_vector_sub(yprev,ynow);            //same way to find c as i two_step, and memcopy to y
	gsl_vector_scale(ydiff,(t-tprev));  
	gsl_vector_add(yprev,ydiff);     	    
	gsl_vector_scale(yprev,pow((t-tprev),-2)); 
	gsl_vector_memcpy(yprev,y);
	
	//Second order value
	f(t,ynow,ydiff);                                      //reset yi'
	gsl_vector_scale(ydiff,h);                           //y'*(t+h-t) = y'*h->y' (by inserting xi+1 = xi +h in 27)
	gsl_vector_scale(yprev,pow(h,2));                   //y*(t+h-t)² -> c(yprev)
	gsl_vector_add(yprev,ydiff);                       //y'*h +c*h² -> c(yprev)
	gsl_vector_add(yprev,ynow);                       //yi + y'h + ch² -> c(yprev)

	//Finding coefficient d (32) and (30)
	double bottom = 2*(h)*(t+h-tprev)+pow(h,2);     //denominator
	gsl_vector_scale(y,2*h);                       //2y*(t+h-t);
	f(t,yprev,ynew);                              //Second order function y'' = ft -> ynew
	f(t,ynow,ydiff);              	             //Reset to y' = f(xi,yi)
       	gsl_vector_sub(ydiff,y);                    //y' -2yh -> y'
	gsl_vector_sub(ynew, ydiff);               //y'' - y' -> ynew (numerator)
	gsl_vector_scale(ynew,pow(bottom,-1));    // numerator (ynew)/denominator (bottom). -> ynew = d

	//Third order value
	gsl_vector_scale(ynew,(h*h)*(t-tprev)); //d*(x-xi)²*(x-xi-1) when inserting in 30
	gsl_vector_add(ynew,yprev);            //new y with d 30

	//Estimate error
	gsl_vector_memcpy(err,ynew);
	gsl_vector_sub(err,yprev);          // 33 

	gsl_vector_free(ydiff);
	gsl_vector_free(y);
	

}
