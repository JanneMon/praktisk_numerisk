#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include<math.h>
#include<stdio.h>

double inner_product(gsl_matrix* A, gsl_matrix* B, int i, int j);
void solve(const gsl_matrix* Q,const gsl_matrix* R, gsl_vector* b, gsl_vector *x);

//this function creates an n,m matrix consisting of random numbers. 
void magic(gsl_matrix* A){

	for(int n = 0; n < A->size1; n++){
		for(int m = 0; m < A->size2; m++){
			gsl_matrix_set(A,n,m,rand() % 10+1);
		}
	}
}

void matrix_decomp(gsl_matrix* A, gsl_matrix* Q, gsl_matrix* R){
	
	//parameters
	int n = A->size1;
	int m = A->size2;
	int i = 0;
	int j = 0;
	int l = 0;	
	
	//the following for-loops creates R and Q as an upper diagonal matrix//
	//Everytime a gsl_matrx_get or set includes a j and an i it is the Xij'th entrance. If it only contains either j or i, it is a coloumn. 
	for(i= 0; i < m;i++){
		//Calculating the diagonal in R//
		double Rii = sqrt(inner_product(A,A,i,i));
		gsl_matrix_set(R,i,i,Rii);

		//Calculating the qi'th coloumn in Q//
		for(l = 0; l<n; l++){
			double A_R = gsl_matrix_get(A,l,i)/gsl_matrix_get(R,i,i);
			gsl_matrix_set(Q,l,i,A_R);	
		}
		for(j = i+1; j<m; j++){
			//Calculating the rest of the entrances in R
			gsl_matrix_set(R,i,j,inner_product(Q,A,i,j));
			// 
			for(l = 0; l<n; l++){
				double Aj = gsl_matrix_get(A,l,j) - gsl_matrix_get(Q,l,i)*gsl_matrix_get(R,i,j);
			        gsl_matrix_set(A,l,j,Aj);	
			}
		}
	}
	
}
//the following algorithm calculates the inverse matrix of A, by using eq. 44 in the notes. It creates the unit vectors and creates a matrix B that fulfills AB = I. The coloumns in B consist of the vectors x that solves QRx = b where b is a set of unit vectors. 
void inverse(const gsl_matrix* Q, const gsl_matrix* R, gsl_matrix* B){
	int dim = R->size1;
	gsl_vector* b = gsl_vector_alloc(dim);
	gsl_vector* x = gsl_vector_alloc(dim);

	for(int i = 0;i<dim; i++){
		gsl_vector_set(b,i,1.0);
		solve(Q,R,b,x);
		gsl_vector_set(b,i,0.0);
		gsl_matrix_set_col(B,i,x);
	}
	gsl_vector_free(b);
	gsl_vector_free(x);
}	

//This function calculates the inner product of two matrices (coloumn by coloumn)
double inner_product(gsl_matrix* A, gsl_matrix* B, int i, int j){
	double product = 0.0;
	int n = A->size1;
	int l = 0;

	for(l = 0; l<n; l++){
		product += gsl_matrix_get(A,l,i)*gsl_matrix_get(B,l,j);
	}
	return product;
}

// the following functions prints matrix and vector
void print_matrix(const char* s, gsl_matrix* A){
	fprintf(stderr,"\n%s\n",s);
	for(int j = 0; j<A->size1;j++){
		for(int i = 0; i<A->size2; i++){
			fprintf(stderr, "%8.3f ",gsl_matrix_get(A,j,i));
		}
		fprintf(stderr,"\n");
	}
	
}

void print_vector(const char* s, gsl_vector* b){
	fprintf(stderr,"\n%s\n",s);
	for(int i = 0; i<b->size; i++){
		fprintf(stderr,"%8.3f",gsl_vector_get(b,i));
		fprintf(stderr,"\n");
	}
}

void solve( const gsl_matrix* Q , const gsl_matrix* R, gsl_vector* b, gsl_vector *x){
	// solving the QRx = b by Q^T*b = c
	gsl_vector* c = gsl_vector_alloc(b->size);
	gsl_blas_dgemv(CblasTrans,1, Q,b,0,c);
	int n = c-> size-1;
	double sub;
	
	// Back substitution
	for(int i = n; i>=0; i-- ){
		sub = gsl_vector_get(c,i);

		for(int k = i+1; k<b->size; k++){
			sub-=gsl_matrix_get(R,i,k) * gsl_vector_get(x,k);
		}
		gsl_vector_set(x,i,sub/gsl_matrix_get(R,i,i));
	}
	gsl_vector_free(c);
	
}

	
void vector_magic(gsl_vector* b){

	for(int n = 0; n < b->size; n++){
			gsl_vector_set(b,n,rand() % 10+1);
	}
}


void symmetric_magic(gsl_matrix* A){
	// Symmetrical, hence n = m
	for(int i = 0; i < A->size1; i++){
		for(int j = 0; j < A->size1; j++){
			double randnum = rand() % 10+1;
			gsl_matrix_set(A,i,j,randnum);
			gsl_matrix_set(A,j,i,randnum);
		}
	}
}	
