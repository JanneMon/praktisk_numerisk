#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<stdlib.h>
#include<stdio.h>
#include<gsl/gsl_blas.h>
#include<math.h>

int jacobi(gsl_matrix* A, gsl_vector* e, gsl_matrix* V);
void symmetric_magic(gsl_matrix* A);
void print_matrix(const char* s, gsl_matrix* A);
void print_vector(const char* s, gsl_vector* b);
void check(gsl_matrix* A, gsl_matrix* B, gsl_matrix* BTAB);

int main(int argc,const char *argv[]){
	
	int n;

	// time it takes to compile
	if(argc > 1){
		n = atoi(argv[1]);
	}else{
		n = 7;
	}

	gsl_matrix* A     = gsl_matrix_alloc(n,n);
	gsl_matrix* Acopy = gsl_matrix_alloc(n,n);
	gsl_matrix* V     = gsl_matrix_alloc(n,n);
	gsl_vector* e     = gsl_vector_alloc(n);

	symmetric_magic(A);
	gsl_matrix_memcpy(Acopy, A);
	print_matrix("Random symmetrical matrix A",A);

	int sweeps = jacobi(A,e,V);
	fprintf(stderr,"Diagonalization done in %i sweeps\n",sweeps);
	print_vector("This yilds eigenvalues",e);
	print_matrix("Hence, the matrix of eigenvectors is",V);
	
	gsl_matrix* D = gsl_matrix_alloc(n,n);
	for(int i = 0; i < n; i++){
		gsl_matrix_set(D,i,i,gsl_vector_get(e,i));
	}


	//check that VTAV == D
	gsl_matrix* AV = gsl_matrix_alloc(n,n);
	check(Acopy,V,AV);
	fprintf(stderr,"Checking that V^TAV == D:\n");
	print_matrix("VTAV should give:",D);
	print_matrix("It gives:",AV);

	//free matrices and vectors
	gsl_matrix_free(A);
	gsl_matrix_free(V);
	gsl_matrix_free(Acopy);
	gsl_vector_free(e);
	gsl_matrix_free(AV);


}

void check(gsl_matrix*A, gsl_matrix* B, gsl_matrix* BTAB){
	int n = A->size1;
	gsl_matrix* AB = gsl_matrix_alloc(n,n);
	gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1, A,B,0.0,AB);
	gsl_blas_dgemm(CblasTrans, CblasNoTrans,1, B,AB,0.0,BTAB);
	gsl_matrix_free(AB);
}
