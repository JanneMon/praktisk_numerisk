#include<math.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include<stdio.h>
#include<stdlib.h>

int jacobi(gsl_matrix* A,gsl_vector *e, gsl_matrix *V);
int jacobiLowHigh(gsl_matrix* A, gsl_vector* e, gsl_matrix* V, int numvals);
int jacobiHighLow(gsl_matrix* A, gsl_vector* e, gsl_matrix* V, int numvals);

int get_rotations();
int reset_rot();
void symmetric_magic(gsl_matrix* A);
void print_matrix(const char* s,gsl_matrix* A);
/*double my_rand(){
	return ((double)rand()/RAND_MAX);
}*/

int main(){
	int n = 7;
	int nvals = 1;

	//time_t t;
	//srand((unsigned) time(&t));

	gsl_matrix *A      = gsl_matrix_calloc(n,n);
	gsl_matrix* Acopy  = gsl_matrix_calloc(n,n);
	gsl_matrix* Acopy2 = gsl_matrix_calloc(n,n);
	gsl_matrix* Acopy3 = gsl_matrix_calloc(n,n);

	/*for(int i = 0; i<n; i++){
		for(int j = 0; j<n; j++){
			double Aij = symmetric_magic(A);
			gsl_matrix_set(A,i,j,Aij);
			gsl_matrix_set(A,j,i,Aij);

	}*/ 
	
	symmetric_magic(A);
	//fprintf(stde"\nSuccess\n");

	gsl_matrix_memcpy(Acopy,A);
	gsl_matrix_memcpy(Acopy2,A);
	gsl_matrix_memcpy(Acopy3,A);

	print_matrix("Matrix A:",A);

	gsl_matrix* V = gsl_matrix_calloc(n,n);
	gsl_matrix* Vcheck = gsl_matrix_calloc(n,n);

	gsl_vector* e      = gsl_vector_alloc(n);
	gsl_vector* eH     = gsl_vector_alloc(n);
	gsl_vector* echeck = gsl_vector_alloc(n);
	gsl_vector* all    = gsl_vector_alloc(n);

	int testsweeps = jacobi(A,echeck,Vcheck);
	int testrots   = get_rotations();

	reset_rot();

	int Lowsweeps = jacobiLowHigh(Acopy,e,V,nvals);
	int rots = get_rotations();

	reset_rot();

	int Highsweeps = jacobiHighLow(Acopy2,eH,V,nvals);
	int rotsH = get_rotations();

	reset_rot();

	int Allsweeps = jacobiHighLow(Acopy3,all,V,n);
	int rotsall = get_rotations();
	
	fprintf(stderr,"\n________________________________________________\n");
	fprintf(stderr,"Lowest eigenvalue\n");
	gsl_vector_fprintf(stderr,e,"%7.3f");
	fprintf(stderr,"Number of rotations: %i, sweeps: %i\n",rots,Lowsweeps);

	fprintf(stderr,"Highest eigenvalue\n");
	gsl_vector_fprintf(stderr,eH,"%7.3f");
	fprintf(stderr,"Number of rotations: %i, sweeps: %i\n",rotsH,Highsweeps);

	fprintf(stderr,"All eigenvalues value by value\n");
	gsl_vector_fprintf(stderr,all,"%7.3f");
	fprintf(stderr,"Number of rotations: %i, sweeps: %i\n",rotsall,Allsweeps);

	fprintf(stderr,"All eigenvalues (cyclic)\n");
	gsl_vector_fprintf(stderr,echeck,"%7.3f");
	fprintf(stderr,"Number of rotations: %i, sweeps: %i\n",testrots,testsweeps);

 	gsl_vector_free(e);
	gsl_vector_free(echeck);
 	gsl_vector_free(eH);
 	gsl_vector_free(all);
 	gsl_matrix_free(A);
 	gsl_matrix_free(Acopy);
 	gsl_matrix_free(Acopy2);
 	gsl_matrix_free(Acopy3);
  	gsl_matrix_free(V);
	gsl_matrix_free(Vcheck);

return 0;
}
