#include<gsl/gsl_vector.h>
#include<stdio.h>
#include"neurons.h"
#include<math.h>


double activation_function(double x){return exp(-x*x);}
double function_to_fit(double x){return cos(5*x)*exp(-x*x);}

int main(){
	fprintf(stderr, "\n_____________________________________________________________________________\n");
	fprintf(stderr, "\n A. Artificial Neural trained to interpolate functions\n");
	fprintf(stderr, "_____________________________________________________________________________\n");

	int n=20;
	int nx = 20;

	neurons* nw=neurons_alloc(n, activation_function);
	double a=-1,b=1;
	gsl_vector* vx = gsl_vector_alloc(nx);
	gsl_vector* vf = gsl_vector_alloc(nx);

	for(int i = 0;i<nx;i++){
		double x = a+(b-a)*i/(nx-1);
		double f = function_to_fit(x);
		gsl_vector_set(vx,i,x);
		gsl_vector_set(vf,i,f);
	}
	
	
	for(int i = 0;i<nw->n;i++){
		gsl_vector_set(nw->data, 3*i+0,a+(b-a)*i/(nw->n-1.0));
		gsl_vector_set(nw->data, 3*i+1,1);
		gsl_vector_set(nw->data, 3*i+2,1);
	}
	neurons_train(nw,vx,vf);
	for(int i = 0; i<vx->size; i++){
		double x = gsl_vector_get(vx,i);
		double f = gsl_vector_get(vf,i);
		printf("%-20.6g \t %-20.6g\n",x,f);

	}
	printf("\n\n");
	
	double dz = 1.0/64;
	for(double z = a;z<=b;z+=dz){
		double y = neurons_feed_forward(nw,z);
		printf("%-20.6g \t %-20.6g\n",z,y);
	}
neurons_free(nw);
gsl_vector_free(vx); gsl_vector_free(vf);
return 0;
}
