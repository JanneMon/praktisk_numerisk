#include"neurons.h"
#include<gsl/gsl_vector.h>
#include<stdio.h>
#include<gsl/gsl_matrix.h>
#include<stdio.h>
#include<math.h>
#include<gsl/gsl_blas.h>
#include<gsl/gsl_multimin.h>

double qnewton(double phi(gsl_vector* x), gsl_vector* x, double eps);

neurons* neurons_alloc(int n, double(*f)(double)){
	neurons* nw = malloc(sizeof(neurons));
	nw->n=n;
	nw->f=f;
	nw->data = gsl_vector_alloc(3*n);
	return nw;
}
void neurons_free(neurons* nw){
	gsl_vector_free(nw->data);
	free(nw);
}
double neurons_feed_forward(neurons* nw, double x){
	double s=0;
	for(int i = 0; i<nw->n;i++){
		double a = gsl_vector_get(nw->data,3*i+0);
		double b = gsl_vector_get(nw->data,3*i+1);
		double w = gsl_vector_get(nw->data,3*i+2);
		s+=nw->f((x+a)/b)*w;
	}
	return s;
}

void neurons_train(neurons* nw, gsl_vector* vx, gsl_vector* vf){
	int n = 3*nw->n;
	gsl_vector* v = gsl_vector_alloc(n);
	gsl_vector* startstep = gsl_vector_alloc(n);

#define DIFF(x) pow(x,2)
	double score(const gsl_vector *v, void *params){
		gsl_vector_memcpy(nw->data,v);


		double s=0;
		for(int i =0;i<vx->size;i++){
			double x=gsl_vector_get(vx,i);
			double f=gsl_vector_get(vf,i);
			double y=neurons_feed_forward(nw,x);
			s+=DIFF(y-f);
		}
		s/= vx->size;
		return s;
	}	
	
	gsl_vector_memcpy(v,nw->data);
	gsl_multimin_function F = {.f=score, .n=n, .params=NULL};
	gsl_vector_set_all(startstep,0.1);

	gsl_multimin_fminimizer *minimizer = gsl_multimin_fminimizer_alloc(gsl_multimin_fminimizer_nmsimplex2,n);
	gsl_multimin_fminimizer_set(minimizer,&F,v,startstep);

	int iter = 0;
	int flag;

	do{
		iter++;
		flag = gsl_multimin_fminimizer_iterate(minimizer);
		if(flag !=0){
			fprintf(stderr,"%i iterations done cannot be improved. \n",iter);
			break;
		}
	}while(iter < 100000);

	gsl_vector_memcpy(nw->data, minimizer->x);
	
	gsl_vector_free(v);
	gsl_vector_free(startstep);
	gsl_multimin_fminimizer_free(minimizer);
}


