#include<assert.h>
#include<stdlib.h>
#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_multimin.h>


//In part b neurons = ann2d. 
typedef struct {
	int n;
	double (*f)(double,double);
	gsl_vector* data;
}ann2d;

ann2d* ann2d_alloc(int n, double(*f)(double,double)){
	ann2d* nw = malloc(sizeof(ann2d));
	nw->n=n;
	nw->f=f;
	nw->data = gsl_vector_alloc(5*n); //5 because we are now in 2d
	return nw;
}
void ann2d_free(ann2d* nw){
	gsl_vector_free(nw->data);
	free(nw);
}
double ann2d_feed(ann2d* nw, double x, double y){
	double nots = 0;
	for(int i = 0; i<nw->n;i++){
		double a = gsl_vector_get(nw->data,0*nw->n+i);
		double b = gsl_vector_get(nw->data,1*nw->n+i);
		double c = gsl_vector_get(nw->data,2*nw->n+i);
		double d = gsl_vector_get(nw->data,3*nw->n+i);
		double w = gsl_vector_get(nw->data,4*nw->n+i);
		nots+=nw->f((x-a)/b,(y-c)/d)*w;
	}
	return nots;
}

void ann2d_train(ann2d* nw, gsl_vector* vx, gsl_vector* vf, gsl_matrix* vz){
	int n = nw->data->size;
	gsl_vector* v = gsl_vector_alloc(n);
	gsl_vector* startstep = gsl_vector_alloc(n);

#define DIFF(x) pow(x,2)
	double score(const gsl_vector *v, void *params){
		gsl_vector_memcpy(nw->data,v);

		double s=0;
		for(int i =0;i<vx->size;i++){
			for(int j =0; j<vf->size;j++){
				double x=gsl_vector_get(vx,i);
				double y=gsl_vector_get(vf,j);
				double z=gsl_matrix_get(vz,i,j);
				double fit = ann2d_feed(nw,x,y);
			s+=DIFF(z-fit);
			}
		}
		s/= vx->size*vf->size;
		return s;
	}	
	
	gsl_vector_memcpy(v,nw->data);
	gsl_multimin_function F = {.f=score, .n=n, .params=NULL};
	gsl_vector_set_all(startstep,0.1);

	gsl_multimin_fminimizer *minimizer = gsl_multimin_fminimizer_alloc(gsl_multimin_fminimizer_nmsimplex2,n);
	gsl_multimin_fminimizer_set(minimizer,&F,v,startstep);

	int iter = 0;
	int flag;

	do{
		iter++;
		flag = gsl_multimin_fminimizer_iterate(minimizer);
		if(flag !=0){
			fprintf(stderr,"%i iterations done cannot be improved. \n",iter);
			break;
		}
		
		if(minimizer->size < 1e-5){
			break;
		}

	}while(iter < 100000);

	gsl_vector_memcpy(nw->data, minimizer->x);
	
	gsl_vector_free(v);
	gsl_vector_free(startstep);
	gsl_multimin_fminimizer_free(minimizer);
}


