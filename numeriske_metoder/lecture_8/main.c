#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#include<assert.h>
#include<tgmath.h>
#include<gsl/gsl_integration.h>

double integrator24(double f(double), double a, double b, double acc, double eps, double f2, double f3, double *integ_err, int reccalls);

double integrator(double f(double), double a, double b, double acc, double eps, double *integ_err);

double infintegrator(double f(double), double a, double b, double acc, double eps, double*err);

double ClenshawCurtis(double f(double), double a, double b, double acc, double eps, double *err);

int calls = 0;
double invsqrt(double x){
	calls ++;
	return 1.0/sqrt(x);	
}

double sqrtx(double x){
	calls ++;
	return sqrt(x);
}

double lnx(double x){
	calls++;
	return log(x)/sqrt(x);
}

double power(double x){
	calls ++;
	return 4*sqrt(1-pow((1-x),2));
}

double gauss(double x){
	calls ++;
	return exp(-x*x);
}

double gauss_gsl(double x,void* params){
	calls++;
	return exp(-x*x);
}
//1/(x²+a²) for a=2
double radius(double x){
	calls ++;
	return 1/(x*x+2*2);
}

double radius_gsl(double x, void* params){
	calls ++;
	return 1/(x*x+2*2);
}


int main(){

	double a = 0;
	double b = 1;

	double acc = 1e-6;
	double eps = 1e-6;
	double error;
//___________________________________PART A___________________________________________
//
	fprintf(stderr,"\n__________________ADAPTIVE INTEGRATION______________\n");
	fprintf(stderr,"\n---------PART A: Recursive adaptive integrator)-----\n");
	fprintf(stderr,"\n1/sqrt(x) integrated from 0 to 1:\n\n");

	// call integration
	double Q_revsqrt = integrator(invsqrt,a,b,acc,eps,&error);
	fprintf(stderr,"Calculated integral = %.6g \t Exact result = 2\n",Q_revsqrt);
	fprintf(stderr,"Estimated error = %g \t Actual error = %g\n", acc+fabs(Q_revsqrt)*eps, fabs(Q_revsqrt-2.0));
	fprintf(stderr,"Calls = %i\n",calls);

	calls = 0;

	fprintf(stderr,"\nsqrt(x) integrated from 0 to 1:\n\n");
	double Q_sqrt = integrator(sqrtx,a,b,acc,eps,&error);
	fprintf(stderr,"Calculated integral = %.6g \t Exact result = %g\n",Q_sqrt,2.0/3.0);
	fprintf(stderr,"Estimated error = %g \t Actual error = %g\n", acc+fabs(Q_sqrt)*eps, fabs(Q_sqrt-(2.0/3.0)));
	fprintf(stderr,"Calls = %i\n",calls);

	calls = 0;

	fprintf(stderr,"\nln(x)/sqrt(x) integrated from 0 to 1:\n\n");
	double Q_ln = integrator(lnx,a,b,acc,eps,&error);
	fprintf(stderr,"Calculated integral = %.6g \t Exact result = -4\n",Q_ln);
	fprintf(stderr,"Estimated error = %g \t Actual error = %g\n", acc+fabs(Q_ln)*eps, fabs(Q_ln+4));
	fprintf(stderr,"Calls = %i\n",calls);

	calls = 0;

	fprintf(stderr,"\n4*sqrt(1-(1-x)²)) integrated from 0 to 1:\n\n");
	double Q_power = integrator(power,a,b,acc,eps,&error);
	fprintf(stderr,"Calculated integral = %.6g \t Exact result = %g\n",Q_power, M_PI);
	fprintf(stderr,"Estimated error = %g \t Actual error = %g\n", acc+fabs(Q_power)*eps, fabs(Q_power-M_PI));
	fprintf(stderr,"Calls = %i\n",calls);

//_____________________________________PART B_________________________________________________________________

	calls = 0;
	
	fprintf(stderr,"\n---------PART B: Generalizing to infinite integrals-----\n");

	fprintf(stderr,"\nexp(-x²) integrated from -inf  to inf:\n\n");
	double Q_gauss = infintegrator(gauss, -INFINITY,INFINITY,acc,eps,&error);
	fprintf(stderr,"Calculated integral = %.6g \t Exact result = %g\n",Q_gauss,sqrt(M_PI));
	fprintf(stderr,"Estimated error = %g \t Actual error = %g\n", acc+fabs(Q_gauss)*eps, fabs(Q_gauss-sqrt(M_PI)));
	fprintf(stderr,"Calls = %i\n",calls);

	calls = 0;
	
	size_t limit = 1e3;
	gsl_integration_workspace* ws = gsl_integration_workspace_alloc(limit);
	gsl_function F;
	double Q_gsl, err_gsl;
       	F.function = &gauss_gsl;
	gsl_integration_qagi(&F,acc,eps,limit, ws, &Q_gsl,&err_gsl);

	fprintf(stderr,"Now using gsl:\n");
	fprintf(stderr,"Calculated integral %g \t Exact result = %g\n",Q_gsl,sqrt(M_PI));
	fprintf(stderr,"Estimated error %g \t Actual error = %g\n", acc+fabs(Q_gsl)*eps,fabs(Q_gsl-sqrt(M_PI)));
	fprintf(stderr,"Calls %i\n",calls);

	calls = 0;

	// Another function 1/x²+a²
	fprintf(stderr,"\n 1/(x²+a²) integrated from 0  to inf:\n\n");
	double Q_radius = infintegrator(radius,0,INFINITY,acc,eps,&error);
	fprintf(stderr,"Calculated integral = %.6g \t Exact result = %g\n",Q_radius,M_PI/4);
	fprintf(stderr,"Estimated error = %g \t Actual error = %g\n", acc+fabs(Q_radius)*eps, fabs(Q_radius-M_PI/4));
	fprintf(stderr,"Calls = %i\n",calls);

	calls = 0;
	
	//size_t limit = 1e3;
	gsl_integration_workspace* ws2 = gsl_integration_workspace_alloc(limit);
	gsl_function G;
	Q_gsl = 0;
	err_gsl = 0;
       	G.function = &radius_gsl;
	gsl_integration_qagil(&G,a,acc,eps,limit, ws2, &Q_gsl,&err_gsl);

	//Prints
	fprintf(stderr,"Now using gsl:\n");
	fprintf(stderr,"Calculated integral %g \t Exact result = %g\n",Q_gsl,M_PI/4);
	fprintf(stderr,"Estimated error %g \t Actual error = %g\n", acc+fabs(Q_gsl)*eps,fabs(Q_gsl-M_PI/4));
	fprintf(stderr,"Calls %i\n",calls);

//____________________________________________PART C____________________________________________________________
 

	fprintf(stderr,"\n---------PART C: Implementing Clenshaw Curtis-------------\n\n");
  	fprintf(stderr,"\n4*sqrt(1-pow((1-x),2)) integrated with Clenshaw-Curtis:\n");
 	double QCCtest = ClenshawCurtis(power, a, b, acc, eps, &error);
 	fprintf(stderr,"Calculated = %.6g \t Exact = %g\n", QCCtest,M_PI);
 	fprintf(stderr,"Error estimate = %.6g\t Actual error = %g\n",acc+fabs(QCCtest)*eps, fabs(QCCtest-M_PI));
	fprintf(stderr,"Calls = %i\n\n",calls);

	calls = 0;

	fprintf(stderr,"InvSqrt integrated with Clenshaw-Curtis:\n");
        double QCCinvsqrt = ClenshawCurtis(invsqrt, a, b, acc, eps, &error);
        fprintf(stderr,"Calculated = %.6g \t Exact = %g\n", QCCinvsqrt, 2.0);
        fprintf(stderr,"Error estimate = %.6g\t Actual error = %g\n", acc+fabs(QCCinvsqrt)*eps,fabs(QCCinvsqrt-2.0));
	fprintf(stderr,"Calls = %i\n",calls);
	return 0;
}
