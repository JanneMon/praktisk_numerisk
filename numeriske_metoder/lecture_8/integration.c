#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#include<assert.h>

int maxcalls = 1000;

double integrator24(double f(double), double a, double b, double acc, double eps, double f2, double f3, double* integ_err, int reccalls){
	
	assert(reccalls<maxcalls);

	double f1 = f(a+(b-a)/6);
	double f4 = f(a+5*(b-a)/6);

	/* weighted sum with wi */
	double Q_good = (2*f1+f2+f3+2*f4)/6 * (b-a);
	double Q_bad = (f1+f2+f3+f4)/4 *(b-a);

	double tol = acc+eps*fabs(Q_good);
	double error = fabs(Q_good - Q_bad);

	if(error < tol){
		*integ_err = error;
		return Q_good;
	}else{
		double integ_err1;
		double integ_err2;

		double Q1 = integrator24(f,a,(a+b)/2, acc/sqrt(2),eps,f1,f2,&integ_err1, reccalls+1);
		double Q2 = integrator24(f,(a+b)/2,b, acc/sqrt(2),eps,f3,f4,&integ_err2, reccalls+1);
		
		*integ_err = sqrt(integ_err1*integ_err1+integ_err2*integ_err2);
		return Q1 + Q2;
	}

}
double integrator(double f(double), double a, double b, double acc, double eps, double *integ_err){
	
	int reccalls = 0;
	if(a == 0){a = 1e-6;}
	double f2 = f(a+2*(b-a)/6);
	double f3 = f(a+4*(b-a)/6);

	double Q = integrator24(f,a,b,acc,eps,f2,f3,integ_err,reccalls);

	return Q;
}

double infintegrator(double f(double), double a, double b, double acc, double eps,double* err){
	int ainf = isinf(a);
	int binf = isinf(b);

	double Q;

	if(ainf == 0 && b==0){
		Q = integrator(f,a,b,acc,eps,err);
	}else if(ainf != 0 && binf !=0){
		double Qinf(double t){return f(t/(1-t*t))*(1+t*t)/((1-t*t)*(1-t*t));}//57
		Q = integrator(Qinf,-1,1,acc,eps,err);	
	}else if(ainf == 0 && binf != 0){
		double Qinf2(double t){return f(a+(1-t)/t)/(t*t);}//60
		Q = integrator(Qinf2,0,1,acc,eps,err);
	}else if(ainf != 0 && binf == 0){
		double Qinf3(double t){return f(b-(1-t)/t)/(t*t);}//62
		Q = integrator(Qinf3,0,1,acc,eps,err);
	}else{
		printf("Error, could not integrate infinite integral!\n");
		Q = INFINITY;
	}
	return Q;
}

double ClenshawCurtis(double f(double), double a, double b, double acc, double eps, double *err){
	double g(double t){
		return f((a+b)/2+(a-b)/2*cos(t))*sin(t)*(b-a)/2;
	}
	return integrator(g,0,M_PI,acc,eps,err);
}
