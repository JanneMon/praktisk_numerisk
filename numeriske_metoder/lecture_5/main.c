#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<stdio.h>
#include<math.h>

int newton(void f(gsl_vector* p, gsl_vector* fx), gsl_vector* x, double dx, double eps);

void vector_print(const char* s, gsl_vector* v);
void rosenbrock(gsl_vector* p, gsl_vector* fx);
void himmelblau(gsl_vector* p, gsl_vector* fx);
void soe(gsl_vector* p, gsl_vector* fx);
void reset(gsl_vector* x, int x1, int x2);

int main(){
	int dim    = 2;
	double eps = 1e-3;
	double dx  = 1e-6;	

	//Set initial vectors
	gsl_vector* x = gsl_vector_alloc(dim);
	gsl_vector* fx = gsl_vector_alloc(dim);
	
	//Set initial start guess
	gsl_vector_set(x,0,-2);
	gsl_vector_set(x,1,2);
	
	
	fprintf(stderr,"Rootfinding:\n\n");
	//Solve the system of equations
	soe(x,fx);
	fprintf(stderr,"Solution to system of equations:\n\n");
	fprintf(stderr,"\nIntital conditions for sytem of equations:\n");
	vector_print("x=",x);
	vector_print("fx=",fx);
	newton(soe,x,dx,eps);
	fprintf(stderr,"\nSolution:\n");
	vector_print("x =",x);
	
	//test that function is xero in the solution
	soe(x,fx);
	vector_print("f(x) =" ,fx);
	fprintf(stderr,"\n\n");
	
	reset(x,-2,2);

	//Solve the Rosenbrock
	rosenbrock(x,fx);
	fprintf(stderr,"\nIntital conditions for Rosenbrock:\n");
	vector_print("x=",x);
	vector_print("fx=",fx);
	newton(rosenbrock,x,dx,eps);
	fprintf(stderr,"\nSolution:\n");
	vector_print("x =",x);
	

	rosenbrock(x,fx);
	vector_print("f(x) =" ,fx);
	fprintf(stderr,"\n\n");
	
	reset(x,-1,1); 

	//Solve the Himmelblau 
	himmelblau(x,fx);
	fprintf(stderr,"\nIntital conditions for Himmelblau:\n");
	vector_print("x=",x);
	vector_print("fx=",fx);
	newton(himmelblau,x,dx,eps);
	fprintf(stderr,"\nSolution:\n");
	vector_print("x =",x);

	himmelblau(x,fx);
	vector_print("f(x) =" ,fx);
	
	//free the prisoners:
	gsl_vector_free(x);
	gsl_vector_free(fx);
}

void reset(gsl_vector* x,int x1, int x2){
	gsl_vector_set_zero(x);
	gsl_vector_set(x,0,x1);
	gsl_vector_set(x,1,x2);
}
