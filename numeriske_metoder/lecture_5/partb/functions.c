#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<stdio.h>
#include<gsl/gsl_blas.h>
#include<math.h>

void solve(const gsl_matrix* Q , const gsl_matrix* R, gsl_vector* b, gsl_vector* x);
void matrix_decomp(gsl_matrix* A, gsl_matrix* Q, gsl_matrix* R);
double inner_product(gsl_matrix* A, gsl_matrix* B, int i, int j);

void vector_print(const char* s, gsl_vector* v){
	fprintf(stderr,"\n%s ",s);
	for(int i = 0; i<v->size;i++){
		fprintf(stderr,"%8.3f ", gsl_vector_get(v,i));
	}
		fprintf(stderr,"\n");
}

double inner_product(gsl_matrix* A, gsl_matrix* B, int i, int j){
	double product = 0.0;
	int n = A->size1;
	int l = 0;

	for(l = 0; l<n; l++){
		product += gsl_matrix_get(A,l,i)*gsl_matrix_get(B,l,j);
	}
	return product;
}

// Set f(x) vector for rosenbrock function derivatives and puts them into fx
void rosenbrock(gsl_vector* p, gsl_vector* fx, gsl_matrix* J, int J_yesno){
	double x = gsl_vector_get(p,0), y = gsl_vector_get(p,1);
	gsl_vector_set(fx,0,2*(1-x)*(-1)+100*2*(y-x*x)*(-1)*2*x);
	gsl_vector_set(fx,1,100*2*(y-x*x));
	
	if(J_yesno == 0)return;
	else{
		gsl_matrix_set(J,0,0,2-400*(y-3*x*x));
		gsl_matrix_set(J,0,1,-400*x);
		gsl_matrix_set(J,1,0,-400*x);
		gsl_matrix_set(J,1,1,200.0);
	}
}
//set f(x) vector for himmelblau function derivatives and puts them into fx
void himmelblau(gsl_vector* p,gsl_vector* fx, gsl_matrix* J, int J_yesno){
	double x = gsl_vector_get(p,0), y = gsl_vector_get(p,1);
	gsl_vector_set(fx,0, 4*(x*x*x+x*y-11)+2*(x+y*y-7));
	gsl_vector_set(fx,1, 2*(x*x+y-11)+4*y*(x+y*y-7));	
	if (J_yesno == 0)return;
	else{
		gsl_matrix_set(J,0,0,4*(3*x*x+y)+2);
		gsl_matrix_set(J,0,1,4*x+4*y);
		gsl_matrix_set(J,1,0,4*x+4*y);
		gsl_matrix_set(J,1,1,4*x+4*y);
	}
}

void soe(gsl_vector* p, gsl_vector* fx, gsl_matrix* J, int J_yesno){
	int A = 10000;
	double x = gsl_vector_get(p,0), y = gsl_vector_get(p,1);
	gsl_vector_set(fx,0, A*x*y -1);
	gsl_vector_set(fx,1, exp(-x) + exp(-y) - 1.0 - 1.0/A);
	
	if(J_yesno == 0)return;
	else{
		gsl_matrix_set(J,0,0,A*y);
		gsl_matrix_set(J,0,1,A*x);
		gsl_matrix_set(J,1,0,-exp(-x));
		gsl_matrix_set(J,1,1,-exp(-y));
	}
}

void newton(void* f(gsl_vector* p, gsl_vector* fx, gsl_matrix* J, int J_yesno),gsl_vector* x, double dx, double eps, gsl_matrix* J, int J_yesno){
	int n = x->size;
	

/*	
	if(J_yesno == 0){
	gsl_matrix* J  = gsl_matrix_alloc(n,n);
	}*/
	gsl_matrix* R  = gsl_matrix_calloc(n,n);
	gsl_vector* fx = gsl_vector_calloc(n);
	gsl_vector* z  = gsl_vector_calloc(n);
	gsl_vector* fz = gsl_vector_calloc(n);
	gsl_vector* df = gsl_vector_calloc(n);
	gsl_vector* Dx = gsl_vector_calloc(n);
	gsl_matrix* Q  = gsl_matrix_calloc(n,n);
	
	int steps = 0;
	int fcalls = 0;

	while(1){
		fcalls ++;
		steps ++;
		f(x,fx,J,J_yesno);
		if(J_yesno == 0){
		for(int j = 0; j<n;j++){
			//Set x vector as x+dx 
			gsl_vector_set(x,j,gsl_vector_get(x,j)+dx);
			
			fcalls ++;
			f(x,df,J, J_yesno); // get entries for differentiated functions

			gsl_vector_sub(df,fx);// defines df = f(x+dx)-f(x)
			
			//Set jacobian and reset x	
			for(int i = 0; i<n; i++) gsl_matrix_set(J,i,j,gsl_vector_get(df,i)/dx);

				gsl_vector_set(x,j,gsl_vector_get(x,j)-dx); //resets x so that the dx is added per entry
			}}
			matrix_decomp(J,Q,R);
			solve(Q,R,fx,Dx);
			gsl_vector_scale(Dx,-1);
		
			// set end condition, assuming lambda=1;
			double s = 1;
			while(1){
				gsl_vector_memcpy(z,x);
				gsl_vector_add(z,Dx);
				fcalls ++;
				f(z,fz,J, J_yesno);

				if(gsl_blas_dnrm2(fz)<(1-s/2)*gsl_blas_dnrm2(fx) || s<0.02) break;
				s *= 0.5;
				gsl_vector_scale(Dx,0.5);
			}

			gsl_vector_memcpy(x,z);
			gsl_vector_memcpy(fx,fz);
			if(gsl_blas_dnrm2(Dx)<dx || gsl_blas_dnrm2(fx)<eps) break;

			if(steps > 1e4) {fprintf( stderr, "too many steps");
				break;}

			}

//	gsl_matrix_free(J);
       	gsl_matrix_free(R);
	gsl_matrix_free(Q);
	gsl_vector_free(fx);
	gsl_vector_free(z);
	gsl_vector_free(fz);
	gsl_vector_free(df);
	gsl_vector_free(Dx);
	
	fprintf(stderr,"\n Number of function calls: %i\n",fcalls);	
	fprintf(stderr,"\n Number of steps: %i\n",steps);	
}
void matrix_decomp(gsl_matrix* A, gsl_matrix* Q, gsl_matrix* R){
	
	//parameters
	int n = A->size1;
	int m = A->size2;
	int i = 0;
	int j = 0;
	int l = 0;	
	
	//the following for-loops creates R and Q as an upper diagonal matrix//
	//Everytime a gsl_matrx_get or set includes a j and an i it is the Xij'th entrance. If it only contains either j or i, it is a coloumn. 
	for(i= 0; i < m;i++){
		//Calculating the diagonal in R//
		double Rii = sqrt(inner_product(A,A,i,i));
		gsl_matrix_set(R,i,i,Rii);

		//Calculating the qi'th coloumn in Q//
		for(l = 0; l<n; l++){
			double A_R = gsl_matrix_get(A,l,i)/gsl_matrix_get(R,i,i);
			gsl_matrix_set(Q,l,i,A_R);	
		}
		for(j = i+1; j<m; j++){
			//Calculating the rest of the entrances in R
			gsl_matrix_set(R,i,j,inner_product(Q,A,i,j));
			// 
			for(l = 0; l<n; l++){
				double Aj = gsl_matrix_get(A,l,j) - gsl_matrix_get(Q,l,i)*gsl_matrix_get(R,i,j);
			        gsl_matrix_set(A,l,j,Aj);	
			}
		}
	}
}

void solve(const gsl_matrix* Q , const gsl_matrix* R, gsl_vector* b, gsl_vector *x){
	// solving the QRx = b by Q^T*b = c
	gsl_vector* c = gsl_vector_alloc(x->size);
	gsl_blas_dgemv(CblasTrans,1, Q,b,0,c);
	int n = c-> size-1;
	int m = R-> size1;
	double sub;
	
	// Back substitution
	for(int i = n; i>=0; i-- ){
		sub = gsl_vector_get(c,i);

		for(int k = i+1; k<m; k++){
			sub-=gsl_matrix_get(R,i,k) * gsl_vector_get(x,k);
		}
		gsl_vector_set(x,i,sub/gsl_matrix_get(R,i,i));
	}
	gsl_vector_free(c);
	
}
