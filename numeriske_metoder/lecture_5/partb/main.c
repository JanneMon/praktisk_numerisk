#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<stdio.h>
#include<math.h>

int newton(void f(gsl_vector* p, gsl_vector* fx, gsl_matrix* J, int J_yesno), gsl_vector* x, double dx, double eps, gsl_matrix* J, int J_yesno);

void vector_print(const char* s, gsl_vector* v);
void rosenbrock(gsl_vector* p, gsl_vector* fx, gsl_matrix* J, int J_yesno);
void himmelblau(gsl_vector* p, gsl_vector* fx, gsl_matrix* J, int J_yesno);
void soe(gsl_vector* p, gsl_vector* fx, gsl_matrix* J, int J_yesno);
void reset(gsl_vector* x, int x1, int x2);

int main(){
	int dim    = 2;
	double eps = 1e-3;
	double dx  = 1e-6;	

	//Set initial vectors
	gsl_vector* x = gsl_vector_calloc(dim);
	gsl_vector* fx = gsl_vector_calloc(dim);
	gsl_matrix* J = gsl_matrix_calloc(dim,dim);
	
	//Set initial start guess
	gsl_vector_set(x,0,-2);
	gsl_vector_set(x,1,2);
	
	
	fprintf(stderr,"Rootfinding:\n\n");

//*******************************************PART A************************************************	
	//Solve the system of equations
	soe(x,fx,J,0);
	fprintf(stderr,"Solution to system of equations with numerical Jacobian:\n\n");
	fprintf(stderr,"\nIntital conditions for sytem of equations:\n");
	vector_print("x=",x);
	vector_print("fx=",fx);
	newton(soe,x,dx,eps,J,0);
	fprintf(stderr,"\nSolution:\n");
	vector_print("x =",x);
	
	//test that function is xero in the solution
	soe(x,fx,J,0);
	vector_print("f(x) =" ,fx);
	fprintf(stderr,"\n\n");
	
	reset(x,-2,2);

	//Solve the Rosenbrock
	rosenbrock(x,fx,J,0);
	fprintf(stderr,"\nIntital conditions for Rosenbrock:\n");
	vector_print("x=",x);
	vector_print("fx=",fx);
	newton(rosenbrock,x,dx,eps,J,0);
	fprintf(stderr,"\nSolution:\n");
	vector_print("x =",x);
	

	rosenbrock(x,fx,J,0);
	vector_print("f(x) =" ,fx);
	fprintf(stderr,"\n\n");
	
	reset(x,2,2); 

	//Solve the Himmelblau 
	himmelblau(x,fx,J,0);
	fprintf(stderr,"\nIntital conditions for Himmelblau:\n");
	vector_print("x=",x);
	vector_print("fx=",fx);
	newton(himmelblau,x,dx,eps,J,0);
	fprintf(stderr,"\nSolution:\n");
	vector_print("x =",x);

	himmelblau(x,fx,J,0);
	vector_print("f(x) =" ,fx);

	reset(x,-2,2);

//**********************************************************PART B************************************************************''

	//Solve the system of equations with Jacobian matrix. Calling with: 0 numerical jacobian, 1 analytical jacobian
	
	fprintf(stderr,"\n\n\n Solving the system of equations with analystical Jacobian matrix\n\n\n");

	soe(x,fx,J,1); //Can also be one, does not matter. We only need x and fx. 
	fprintf(stderr,"Solution to system of equations:\n\n");
	fprintf(stderr,"\nIntital conditions for sytem of equations:\n");
	vector_print("x=",x);
	vector_print("fx=",fx);
	newton(soe,x,dx,eps,J,1);
	fprintf(stderr,"\nSolution:\n");
	vector_print("x =",x);
	
	soe(x,fx,J,1);
	vector_print("f(x)=",fx);
	fprintf(stderr,"\n\n");
	
	reset(x,2,2); 	


	rosenbrock(x,fx,J,1);
	fprintf(stderr,"\nIntital conditions for Rosenbrock:\n");
	vector_print("x=",x);
	vector_print("fx=",fx);
	newton(rosenbrock,x,dx,eps,J,1);
	fprintf(stderr,"\nSolution:\n");
	vector_print("x =",x);
	

	rosenbrock(x,fx,J,0);
	vector_print("f(x) =" ,fx);
	fprintf(stderr,"\n\n");

	reset(x,2,2);

	himmelblau(x,fx,J,1);
	fprintf(stderr,"\nIntital conditions for Himmelblau:\n");
	vector_print("x=",x);
	vector_print("fx=",fx);
	newton(himmelblau,x,dx,eps,J,1);
	fprintf(stderr,"\nSolution:\n");
	vector_print("x =",x);

	himmelblau(x,fx,J,0);
	vector_print("f(x) =" ,fx);
	fprintf(stderr,"\n\n");

	//free the prisoners:
	gsl_vector_free(x);
	gsl_vector_free(fx);
}

void reset(gsl_vector* x,int x1, int x2){
	gsl_vector_set_zero(x);
	gsl_vector_set(x,0,x1);
	gsl_vector_set(x,1,x2);
}
