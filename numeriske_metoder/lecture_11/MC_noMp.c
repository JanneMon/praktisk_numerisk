#include"math.h"
#include"stdlib.h"
#include<gsl/gsl_rng.h>


void randpoint(int dim, double *a, double *b, double *x, gsl_rng *RNG){
	for(int i = 0; i<dim; i++){
		x[i] = a[i] + gsl_rng_uniform(RNG)* (b[i]-a[i]);
	}
}

void plainmc(int dim, double *a, double *b, double f(double* x), int Numpoints, double* result, double* error){
	
	double V = 1;
	
	for(int i = 0; i<dim; i++){
		V*=b[i]-a[i];
	}

	double sum = 0;
	double sumsq = 0;
	double fx;
	double x[dim];

	gsl_rng * RNG = gsl_rng_alloc(gsl_rng_ranlux389);

	gsl_rng_set(RNG,3);


	for(int i = 0; i< Numpoints; i++){
		randpoint(dim, a, b, x, RNG);
		fx = f(x);
		sum += fx;
		sumsq += fx*fx;
		}


	double avr = sum/Numpoints;
	double var = sum/Numpoints - avr*avr;

	*result = avr*V;
	*error = sqrt(var/Numpoints)*V;

	gsl_rng_free(RNG);
}
