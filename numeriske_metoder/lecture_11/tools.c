void matrix_print(const char* s,const gsl_matrix* m){
	fprintf(stderr,"\n%s\n",s);
	for(int j=0;j<m->size1;j++){
		for(int i=0;i<m->size2;i++){
			fprintf(stderr,"%8.3f ",gsl_matrix_get(m,j,i));
			fprintf(stderr,"\n");
		}
	}
}

void vector_print(const char* s, gsl_vector* v){
	fprintf(stderr,"\n%s ",s);
	for(int i = 0; i<v->size;i++){
		fprintf(stderr,"%8.3f ", gsl_vector_get(v,i));
	}
		fprintf(stderr,"\n");
}


