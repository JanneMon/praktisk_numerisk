#include<math.h>
#include<stdlib.h>
#include<gsl/gsl_rng.h>


void randpoint(int dim, double *a, double *b, double *x, gsl_rng *RNG){
	for(int i = 0; i<dim; i++){
		x[i] = a[i] + gsl_rng_uniform(RNG)* (b[i]-a[i]);
	}
}

void plainmcMP(int dim, double *a, double *b, double f(double* x), int Numpoints, double* result, double* error){
	
	double V = 1;
	
	for(int i = 0; i<dim; i++){
		V*=b[i]-a[i];
	}

	double sum1 = 0;
	double sumsq1 = 0;
	double fx1;
	double x1[dim];

	double sum2 = 0;
	double sumsq2 = 0;
	double fx2;
	double x2[dim];

	gsl_rng * RNG1 = gsl_rng_alloc(gsl_rng_ranlux389);
	gsl_rng * RNG2 = gsl_rng_alloc(gsl_rng_ranlux389);

	gsl_rng_set(RNG1,1);
	gsl_rng_set(RNG2,2);

#pragma omp parallel sections
	{

#pragma omp section
	{

	for(int i = 0; i< Numpoints; i++){
		randpoint(dim, a, b, x1, RNG1);
		fx1 = f(x1);
		sum1 += fx1;
		sumsq1 += fx1*fx1;
		}
	}

#pragma omp section
	{

	for(int i = 0; i<Numpoints; i++){
		randpoint(dim, a, b, x2, RNG2);
		fx2 = f(x2);
		sum2 += fx2;
		sumsq2 += fx2*fx2;
		}
	}
	}

	double avr = (sum1+sum2)/Numpoints;
	double var = (sumsq1 + sumsq2)/Numpoints - avr*avr;

	*result = avr*V;
	*error = sqrt(var/Numpoints)*V;

	gsl_rng_free(RNG1);
	gsl_rng_free(RNG2);
}
