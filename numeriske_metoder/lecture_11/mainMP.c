#include"math.h"
#include<stdio.h>
#include<stdlib.h>

void plainmcMP(int dim, double *a, double *b, double f(double *x), int Numpoints, double *result, double *error);

double gauss(double *x){
	return exp(-x[0]*x[0]-x[1]*x[1]);
}

double intfunction(double *x){
	return 1.0/((1-cos(x[0])*cos(x[1])*cos(x[2]))*M_PI*M_PI*M_PI);
}
double spherical(double *x){
	return x[0]*x[0]*sin(x[1]);
}

int main(int argc, char const *argv[]){
	int dim = 2;
	int Numpoints = 100000;
	double a1[2] = {-10.0, -10.0};
	double b1[2] = {10.0, 10.0};
	double result;
	double error;

	plainmcMP(dim,a1,b1,gauss,Numpoints,&result,&error);
	printf("First function: A 2D gaussian\n");
	printf("Number of samples: %i\n",Numpoints);
	printf("Result: %g\n",result);
	printf("Exact result: %g\n",M_PI);
	printf("Estimated error: %g\n",error);
	printf("Real error: %g\n\n", fabs(result-M_PI));
	
	dim = 3;

	double a3[3]  ={0,0,0};
	double b3[3]  ={M_PI, M_PI,M_PI};
	plainmcMP(dim,a3,b3,intfunction, Numpoints, &result, &error);
	printf("Provided function\n");
	printf("Number of samples: %i\n",Numpoints);
	printf("Result: %g\n",result);
	printf("Exact result: %g\n",1.393203929685);
	printf("Estimated error: %g\n",error);
	printf("Real error: %g\n\n", fabs(result-1.393203929685));
	
	double as[3]  ={0,0,0};
	double bs[3]  ={1, M_PI,2*M_PI};
	plainmcMP(dim,as,bs,spherical, Numpoints, &result, &error);
	printf("Function in spherical coordinates r²sin(theta) \n");
	printf("Number of samples: %i\n",Numpoints);
	printf("Result: %g\n",result);
	printf("Exact result: %g\n",4.1888);
	printf("Estimated error: %g\n",error);
	printf("Real error: %g\n\n", fabs(result-4.1888));
	
 	//PART B: ERROR ESTIMATE FOR PLOTTING
	dim = 2;
	int n_max = 10000;
	for(int j = 100; j<n_max;j+=5){
		plainmcMP(dim,a1,b1,gauss,j, &result,&error);
		fprintf(stderr,"%i \t %g \t %g\n",j,error,fabs(result-M_PI));
	}


	return 0;

}
