#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include<stdio.h>
#include<gsl/gsl_blas.h>
#include<assert.h>
#include<tgmath.h>
#include<math.h>
#define EPS (1.0/1048576)



void solve(const gsl_matrix* Q , const gsl_matrix* R, gsl_vector* b, gsl_vector* x);
void matrix_decomp(gsl_matrix* A, gsl_matrix* Q, gsl_matrix* R);
double inner_product(gsl_matrix* A, gsl_matrix* B, int i, int j);
double himmelblau_function(gsl_vector* p);
double rosenbrock_function(gsl_vector* p);

void vector_print(const char* s, gsl_vector* v){
	fprintf(stderr,"\n%s ",s);
	for(int i = 0; i<v->size;i++){
		fprintf(stderr,"%8.3f ", gsl_vector_get(v,i));
	}
		fprintf(stderr,"\n");
}

double inner_product(gsl_matrix* A, gsl_matrix* B, int i, int j){
	double product = 0.0;
	int n = A->size1;
	int l = 0;

	for(l = 0; l<n; l++){
		product += gsl_matrix_get(A,l,i)*gsl_matrix_get(B,l,j);
	}
	return product;
}


double rosenbrock_function(gsl_vector* p){
	
	double x = gsl_vector_get(p,0), y = gsl_vector_get(p,1);
	
	return ((1-x)*(1-x))+ 100* ( (y-(x*x))*(y-(x*x)));	

}


double rosenbrock(gsl_vector* p, gsl_vector* fx, gsl_matrix* H, int H_yesno){
	
	double x = gsl_vector_get(p,0), y = gsl_vector_get(p,1);
	
	double grad = rosenbrock_function(p);
	
	for(int i = 0; i<p->size; i++){
		
		double xi = gsl_vector_get(p,i);
		double dx = fabs(xi)*EPS;
		
		if(fabs(xi)<sqrt(EPS)) dx = EPS;

		gsl_vector_set(p,i,xi+dx);
		gsl_vector_set(fx, i, (rosenbrock_function(p)-grad)/dx);
		gsl_vector_set(p,i,xi);
	}

	if(H_yesno == 1){
	gsl_matrix_set(H,0,0,2-400*(y-3*x*x));
	gsl_matrix_set(H,0,1,-400*x);
	gsl_matrix_set(H,1,0,-400*x);
	gsl_matrix_set(H,1,1,200.0);
	}
	if(H_yesno == 2){
		return((1-x)*(1-x))+ 100* ( (y-(x*x))*(y-(x*x)));
	}
	return 0;
}


double himmelblau_function(gsl_vector* p){
	
	double x = gsl_vector_get(p,0), y = gsl_vector_get(p,1);

	return (pow(x*x+y-11,2)+pow(x+y*y-7,2));
}


double himmelblau(gsl_vector* p,gsl_vector* fx, gsl_matrix* H, int H_yesno){
	
	double x = gsl_vector_get(p,0), y = gsl_vector_get(p,1);

	double grad = himmelblau_function(p);
	
	for(int i = 0; i<p->size; i++){
		
		double xi = gsl_vector_get(p,i);
		double dx = fabs(xi)*EPS;
		
		if(fabs(xi)<sqrt(EPS)) dx = EPS;

		gsl_vector_set(p,i,xi+dx);
		gsl_vector_set(fx, i, (himmelblau_function(p)-grad)/dx);
		gsl_vector_set(p,i,xi);
	}
	
	if(H_yesno == 1){
	gsl_matrix_set(H,0,0,4*(3*x*x+y)+2);
	gsl_matrix_set(H,0,1,4*x+4*y);
	gsl_matrix_set(H,1,0,4*x+4*y);
	gsl_matrix_set(H,1,1,4*(3*y*y+x)+2);
	}
	if(H_yesno == 2){
		return (pow(x*x+y-11,2)+pow(x+y*y-7,2));
	}
	if(H_yesno == 3){
		gsl_vector_set(p,0, 2* ((-1) + x + 200 * x*x*x - 200 * x * y));
		gsl_vector_set(p,1, 200 *(y-x*x) );
	}
	return 0;
}	


void matrix_decomp(gsl_matrix* A, gsl_matrix* Q, gsl_matrix* R){
	
	//parameters
	int n = A->size1;
	int m = A->size2;
	int i = 0;
	int j = 0;
	int l = 0;	
	
	//the following for-loops creates R and Q as an upper diagonal matrix//
	//Everytime a gsl_matrx_get or set includes a j and an i it is the Xij'th entrance. If it only contains either j or i, it is a coloumn. 
	for(i= 0; i < m;i++){
		//Calculating the diagonal in R//
		double Rii = sqrt(inner_product(A,A,i,i));
		gsl_matrix_set(R,i,i,Rii);

		//Calculating the qi'th coloumn in Q//
		for(l = 0; l<n; l++){
			double A_R = gsl_matrix_get(A,l,i)/gsl_matrix_get(R,i,i);
			gsl_matrix_set(Q,l,i,A_R);	
		}
		for(j = i+1; j<m; j++){
			//Calculating the rest of the entrances in R
			gsl_matrix_set(R,i,j,inner_product(Q,A,i,j));
			// 
			for(l = 0; l<n; l++){
				double Aj = gsl_matrix_get(A,l,j) - gsl_matrix_get(Q,l,i)*gsl_matrix_get(R,i,j);
			        gsl_matrix_set(A,l,j,Aj);	
			}
		}
	}
}

void solve(const gsl_matrix* Q , const gsl_matrix* R, gsl_vector* b, gsl_vector *x){
	// solving the QRx = b by Q^T*b = c
	gsl_vector* c = gsl_vector_alloc(x->size);
	gsl_blas_dgemv(CblasTrans,1, Q,b,0,c);
	int n = c-> size-1;
	int m = R-> size1;
	double sub;
	
	// Back substitution
	for(int i = n; i>=0; i-- ){
		sub = gsl_vector_get(c,i);

		for(int k = i+1; k<m; k++){
			sub-=gsl_matrix_get(R,i,k) * gsl_vector_get(x,k);
		}
		gsl_vector_set(x,i,sub/gsl_matrix_get(R,i,i));
	}
	gsl_vector_free(c);
	
}
