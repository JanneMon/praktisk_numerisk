#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<tgmath.h>
#define EPS (1.0/1048576)

double inner_product(gsl_matrix* A, int i, gsl_matrix* B, int j);
void matrix_decomp(gsl_matrix *A, gsl_matrix* Q, gsl_matrix* R);
void solve(gsl_matrix* Q, gsl_matrix* R, gsl_vector* b, gsl_vector* x);

void qnewton(double f(gsl_vector* p, gsl_vector* grad, gsl_matrix* H, int H_yesno), gsl_vector* x, double eps){
	
	int n = x->size;
	
	gsl_matrix* R     = gsl_matrix_calloc(n,n);
	gsl_vector* gradz = gsl_vector_calloc(n);
	gsl_vector* grad  = gsl_vector_calloc(n);
	gsl_vector* Dx    = gsl_vector_calloc(n);
	gsl_matrix* Q     = gsl_matrix_calloc(n,n);
	gsl_matrix* H     = gsl_matrix_calloc(n,n);
	gsl_vector* z     = gsl_vector_calloc(n);
	gsl_vector* s     = gsl_vector_calloc(n);
	
	int steps = 0;
	int fcalls = 0;
	
	//acoording to Dmititri's notes, one usually approximates the Inverse hessian matric with the identity matrix to a zeroth order approx. 	
	gsl_matrix_set_identity(H);
	f(x,grad,H,0);

	do{

		fcalls ++;
		steps ++;
		
		// solve df(x + s) = df(x) + Hs, for 0'th order approx where dH = 0 and df(x + s) = 0. Thus s = -H^-1 df(x) hence s = -Idf 
		gsl_blas_dgemv(CblasNoTrans,-1.0, H, grad, 0.0,s);
		f(x,grad,H,1);		
		
		double fx = f(x,grad,H,2);
		double lambda = 1.0;
		while(1){

			fcalls++;
			

			gsl_vector_memcpy(z,x);		
			gsl_vector_add(z,Dx);
			
			
			double fz = f(z,gradz,H,2);
			double stg;
			gsl_blas_ddot(Dx, grad, &stg);

			// Equation 9, with Dx being delta x and grad being gradient of f(x). s = lambda * Dx, lambda initially being 1. 
			if((fz < fx + 1e-4*lambda*stg))break;
			if(lambda < 0.02){	
				gsl_matrix_set_identity(H);      
				break; 
			}

			lambda *= 0.5;
			gsl_vector_scale(Dx, 0.5);

		}		
		fcalls++;
		gsl_vector_memcpy(x,z);	
		f(x,grad,H,2);
		steps ++;



	}while(gsl_blas_dnrm2(grad) > eps && steps < 1000);
	fprintf(stderr,"\nsteps = %i, functioncalls = %i\n", steps, fcalls);


       	gsl_matrix_free(R);
	gsl_matrix_free(H);
	gsl_matrix_free(Q);
	gsl_vector_free(grad);
	gsl_vector_free(Dx);
	gsl_vector_free(gradz);
	gsl_vector_free(z);


}
