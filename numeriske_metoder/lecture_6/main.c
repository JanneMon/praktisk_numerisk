#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_blas.h>
#include<assert.h>
#include<tgmath.h>


void vector_print(const char* s, gsl_vector* v);

void newton(double f(gsl_vector* p, gsl_vector* grad, gsl_matrix* H, int H_yesno), gsl_vector* x, double epsilon);
//void qnewton(double f(gsl_vector* p, gsl_vector* grad, gsl_matrix* H, int H_yesno), gsl_vector* x, double epsilon);
double rosenbrock(gsl_vector* p, gsl_vector* fx, gsl_matrix* H, int H_yesno);
double himmelblau(gsl_vector* p, gsl_vector* fx, gsl_matrix* H, int H_yesno);
double rosenbrock_function(gsl_vector* p);
double himmelblau_function(gsl_vector* p);


int main(void){ 
	
	// initial values
	int dim           = 2;
	double epsilon    = 1e-3;
	gsl_vector* x     = gsl_vector_calloc(dim);
	gsl_vector* grad  = gsl_vector_calloc(dim);
		
	fprintf(stderr, "A: Minimization with gradient and Hessian\n");
	fprintf(stderr, "Tolerance (eps) = %g\n\n", epsilon);
	
	// Rosenbrock:
		fprintf(stderr, "Minimization of Rosenbrock\n");
	
		// set vector with initial guess
		gsl_vector_set(x,0, -2);
		gsl_vector_set(x,1, 2);
		vector_print("Startpoint:\t",x);
		newton(rosenbrock, x, epsilon);
		vector_print("endpoint:\t",x);
		double f = rosenbrock_function(x);
		fprintf(stderr, "\nLocal minimum at: %g\n", f);
		fprintf(stderr, "===============================\n\n");	
	
	// Himmelblau:
		fprintf(stderr, "Minimization of Himmelblau\n");
	
		// set vector with initial guess
		gsl_vector_set(x,0, 1);
		gsl_vector_set(x,1, 3);
		vector_print("Startpoint:\t",x);
		newton(himmelblau, x, epsilon);
		vector_print("Endpoint:\t",x);
		double g = himmelblau_function(x);
		fprintf(stderr, "\n Local minimum at: %g\n",g);
		fprintf(stderr, "===============================\n\n");	
	
/*	// Rosenbrock with quasi newton:
		fprintf(stderr, "Minimization of Rosenbrock\n");
	
		// set vector with initial guess
		gsl_vector_set(x,0, -2);
		gsl_vector_set(x,1, 2);
		vector_print("Startpoint:\t",x);
		qnewton(rosenbrock, x, epsilon);
		vector_print("endpoint:\t",x);
		double f_q = rosenbrock_function(x);
		fprintf(stderr, "\n Local minimum at: %g\n",f_q);
		fprintf(stderr, "===============================\n\n");	*/

	// freeeee
	gsl_vector_free(x);
	gsl_vector_free(grad);
	return 0;
}

