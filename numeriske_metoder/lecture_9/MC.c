#include<math.h>
#include<tgmath.h>
#include<stdio.h>
#include<stdlib.h>

double rnd(){
	return ((double)rand()/RAND_MAX);
}

void randpoint(int dim, double *a, double *b, double *x){
	for(int i = 0; i<dim; i++){
		x[i] = a[i] + rnd()* (b[i]-a[i]);
	}
}

void plainmc(int dim, double *a, double *b, double f(double* x), int Numpoints, double* result, double* error){
	
	double V = 1;
	
	for(int i = 0; i<dim; i++){
		V*=b[i]-a[i];
	}

	double sum = 0;
	double sumsq = 0;
	double fx;
	double x[dim];

	for(int i = 0; i< Numpoints; i++){
		randpoint(dim, a, b, x);
		fx = f(x);
		sum += fx;
		sumsq += fx*fx;
	}

	double avr = sum/Numpoints;
	double var = sumsq/Numpoints - avr*avr;

	*result = avr*V;
	*error = sqrt(var/Numpoints)*V;
}
