
______________________________________

------------------PART A--------------

Embedded midpoint euler Runge-Kutta method

Solve y'' = -y for y(0) = 1, and y'(0) = 0
Solved solution is	 y'(1.57079) = -0.00154571	y(1.57079) = -1.00009
Solution is 	 cos(1.57079) = 1.32679e-06	-sin(1.57079)=-1
Acc = 0.001 eps =0.001

_____________________________________

-----------------PART B--------------

           Storing the path          
steps = 22

Solve y'' = -y for y(0) = 1 and y'(0) = 0

 Solved solution is 	 y'(1.57079) = -0.00154571 and y(1.57079) = -1.00009
Exact solution is 	 cos(1.57079) = 1.32679e-06	 -sin(1.57079) = -1
