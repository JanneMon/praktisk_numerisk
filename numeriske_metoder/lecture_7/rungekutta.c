#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_blas.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<math.h>
#include<assert.h>

void driver(
		double *t,                      //target value
		double b,			//end point of integration
		double* h,			//step size
		gsl_vector* yt, 		//current y(t)
		double acc,			//absolute accuracy
		double eps, 			//relative accuracy
		void stepfunc(
			double t, double h, gsl_vector* yt, void f(double t, gsl_vector* y,gsl_vector*dydt),gsl_vector* yth, gsl_vector* err),
		void f(double t, gsl_vector* y, gsl_vector* dydt)
){
	
	int iter = 0, n = yt->size;
	double tol;
	
	gsl_vector* yth = gsl_vector_alloc(n);
	gsl_vector* err = gsl_vector_alloc(n);
	
	do{
		if(fabs(*t + *h) > fabs(b)) *h = b - *t;
		if(*t >=b) break;
		stepfunc(*t, *h, yt,f,yth, err);

		tol = (acc+eps*gsl_blas_dnrm2(yth));
		if(tol > gsl_blas_dnrm2(err)){
			gsl_vector_memcpy(yt, yth);
			iter++;
			*t += *h;
		}
		*h = *h*pow(tol/gsl_blas_dnrm2(err), 0.25)*0.95;
	}while(iter < 1e3);

	gsl_vector_free(yth); gsl_vector_free(err);

}

void stepfunc(
	double t,
	double h,
	gsl_vector* yt,
		void f (double t, gsl_vector* y, gsl_vector* dydt),
		gsl_vector* yth,
		gsl_vector* err
){
	int n = yt->size;

	gsl_vector* k0  = gsl_vector_calloc(n);
	gsl_vector* k12 = gsl_vector_calloc(n);

	gsl_vector_memcpy(yth, yt);

	f(t,yt,k0);
	
	// using the embedded midpoint euler method eq. 25
	// daxpy: y = ax + y
	gsl_blas_daxpy(h/2, k0, yt);	 // y_i+1 = y_i + k0_i + h/2. Sets y value
	f(t+h/2, yt, k12); 	 	 // sets x value 
	gsl_blas_daxpy(h,k12,yth);	 // y_i+1 + h_i = y_i + k12_i * h

	//errors err_i = (k0_i - k12_i) * h
	gsl_vector_sub(k0,k12);
	gsl_vector_scale(k0,h/2);
	gsl_vector_memcpy(err, k0);

	gsl_vector_free(k0); gsl_vector_free(k12);
}

int driver_path(
	gsl_matrix* path,
	double b,
	double* h,
	double acc,
	double eps,
	void stepfunc(
		double t, double h, gsl_vector* yt,
		void f(double t, gsl_vector* y, gsl_vector* dydt),
		gsl_vector* yth, gsl_vector* err
		),
	void f(double t, gsl_vector* y, gsl_vector* dydt)
){
	int n = path->size2-1, max_steps = path->size1, steps = 0;
	double tol, t;
	
	gsl_vector* yth = gsl_vector_alloc(n);
	gsl_vector* err = gsl_vector_alloc(n);
	gsl_vector* y   = gsl_vector_alloc(n);

	while(gsl_matrix_get(path,steps,0) < b){
		t = gsl_matrix_get(path, steps, 0);
		for(int i = 0; i<n; i++){
			gsl_vector_set(y,i,gsl_matrix_get(path,steps,i+1));
		}
		if(fabs(t + *h) > fabs(b)) *h = b - t;
		if(t>=b) break;

		stepfunc(t,*h,y,f,yth,err);
		tol = (acc + eps*gsl_blas_dnrm2(yth));

		if(tol > gsl_blas_dnrm2(err)){
			steps++;
			if(steps+1 > max_steps){
				fprintf(stderr, "\n Max number of steps reached\n");
				break;
			}
			gsl_matrix_set(path,steps,0, t+*h);
			for(int i = 0; i<n; i++){
				gsl_matrix_set(path, steps, i+1, gsl_vector_get(yth,i));
			}
		}
		if(err > 0){
			*h = *h*pow(tol/gsl_blas_dnrm2(err), 0.25)*0.95;
		}
		else *h = 2;
	}
	gsl_vector_free(yth); 
	gsl_vector_free(err);
	return steps;
	
}
