#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_blas.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<math.h>
#include<tgmath.h>
#include<assert.h>

void stepfunc(
	double t,
       	double h,
       	gsl_vector* yt,
       	void f(double t, gsl_vector* y, gsl_vector* dydt), 
	gsl_vector* yth, 
	gsl_vector* err
	);

void driver(
	double *t,
       	double b,
       	double* h,
       	gsl_vector* yt,
       	double acc,
       	double eps,
       	void stepfunc(double t, double h, gsl_vector* yt, 
		void f(double t, gsl_vector* y, gsl_vector* dydt),
	       	gsl_vector* yth, gsl_vector* err
		),
	void f(double t, gsl_vector* y, gsl_vector* dydt)
);

int driver_path(
	gsl_matrix* path, 
	double b,
	double* h,
	double acc,
	double eps,
	void stepfunc(double t, double h, gsl_vector* yt,
		void f(double t, gsl_vector* y, gsl_vector* dydt),
	gsl_vector* yth, gsl_vector* err
	),
	void f(double t, gsl_vector* y, gsl_vector* dydt)
);

// DIFFEQS
//cosine differential function. y gives the non-differentiated value of cos(0) in the first entrance and -sin(0) = 0  in the second entrance (i.e. the differentiated cosine). The couble differentiated is the -cos(x). 

void cosdiff(double x, gsl_vector* y, gsl_vector* dydt){
	gsl_vector_set(dydt, 0, gsl_vector_get(y,1));
	gsl_vector_set(dydt, 1, -gsl_vector_get(y,0));

}

int main(){
	fprintf(stderr, "\n______________________________________\n");
	fprintf(stderr, "\n------------------PART A--------------\n");	
	fprintf(stderr, "\nEmbedded midpoint euler Runge-Kutta method\n");
	
	//Variables
	int n      = 2;
	double a   = 0;
	double b   = 3.14159/2;
	double h;
	double abs = 1e-3;
	double eps = 1e-3;
	
	gsl_vector* y  = gsl_vector_calloc(n);
	
	gsl_vector_set(y,0,1);
	gsl_vector_set(y,1,0);
	h = copysign(0.01, b-a);
	
	driver(&a, b, &h, y, abs, eps, &stepfunc, cosdiff);
	
	fprintf(stderr, "\nSolve y'' = -y for y(0) = 1, and y'(0) = 0\n");
	fprintf(stderr, "Solved solution is\t y'(%g) = %g\ty(%g) = %g\n", a, gsl_vector_get(y,0), a, gsl_vector_get(y,1));
	fprintf(stderr, "Solution is \t cos(%g) = %g\t-sin(%g)=%g\n",a,cos(a),a,-sin(a));
	fprintf(stderr,"Acc = %g eps =%g\n", abs, eps);
	
	// a must be reset, otherwise the driver path and stepper wil star with the "correct" a from the beginning and the number of steps will be zero. 
	
	a = 0;

	fprintf(stderr, "\n_____________________________________\n");
	fprintf(stderr, "\n-----------------PART B--------------\n");
	fprintf(stderr, "\n           Storing the path          \n");
	
	int max_steps = 1e3;
	gsl_matrix* path = gsl_matrix_calloc(max_steps,n+1);
	
	gsl_matrix_set(path,0,0,a);
	gsl_matrix_set(path,0,1,1);
	gsl_matrix_set(path,0,2,0);
	h = copysign(0.01, b-a);

	int steps = driver_path(path,b,&h,abs,eps,&stepfunc,cosdiff);
	fprintf(stderr, "steps = %i\n",steps);

	/* dy/dt = [y' y'']*/

	//fprintf(stdout,"x\t y'\t y = -y''\n");
	for(int i = 0; i<steps; i++){
		fprintf(stdout, "%g\t %g\t %g\n", gsl_matrix_get(path, i,0),gsl_matrix_get(path,i,1), gsl_matrix_get(path,i,2)
			);
	}

	/* y(x) = - y''(x)
	   y'(x) =  y'(x)
	   since 
	   dy cos(0) /dt = -sin(0) = 0
	   dy² cos(0) /dt = -cos(0) = -1 */

	fprintf(stdout,"\n\n");
	fprintf(stderr,"\nSolve y'' = -y for y(0) = 1 and y'(0) = 0\n");
	fprintf(stderr,"\n Solved solution is \t y'(%g) = %g and y(%g) = %g\n", 
			gsl_matrix_get(path,steps,0),
			gsl_matrix_get(path,steps,1),
			gsl_matrix_get(path,steps,0),
		       	gsl_matrix_get(path,steps,2));
	fprintf(stderr,"Exact solution is \t cos(%g) = %g\t -sin(%g) = %g\n",
			gsl_matrix_get(path,steps,0),
			cos(gsl_matrix_get(path,steps,0)), 
			gsl_matrix_get(path,steps,0), 
			-sin(gsl_matrix_get(path,steps,0)));

	return EXIT_SUCCESS;

}
