#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<stdio.h>
#include<math.h>

double inner_product(gsl_matrix* A, gsl_matrix* B, int i, int j);
void matrix_decomp(gsl_matrix* A, gsl_matrix *Q, gsl_matrix* R);
void magic(gsl_matrix *A);
void vector_magic(gsl_vector* b);
void print_matrix(const char *s, gsl_matrix* A);
void print_vector(const char* s, gsl_vector* b);
void solve(gsl_matrix* Q, gsl_matrix* R, gsl_vector* b, gsl_vector* x);
void inverse(gsl_matrix* Q, gsl_matrix* R, gsl_matrix* B);

int main(void){
	fprintf(stderr, "Linear equations \n \n A: solving linear equations using QR-decomposition by Gram-Schmidt orthogonalization \n\n First part: decomposition");

	int n = 9;
	int m = 5;
	
	//Allocate the needed matrices
	gsl_matrix* A  = gsl_matrix_alloc(n,m);
	gsl_matrix* Q  = gsl_matrix_alloc(n,m);
	gsl_matrix* R  = gsl_matrix_alloc(m,m);
	gsl_matrix* QQ = gsl_matrix_alloc(m,m);
       	gsl_matrix* QR = gsl_matrix_alloc(n,m);
	
	//set matrix A with random numbers
	magic(A);
	print_matrix("Random matrix A",A);
	
	// Calculate Q and R by decomposition (follows chapter about QR decomposition and Gram Schmidt process)
	matrix_decomp(A,Q,R);
	print_matrix("Calculated matrix R",R);
	print_matrix("Calculated matrix Q",Q);

	//Checking that QTQ = 1
	gsl_blas_dgemm(CblasTrans, CblasNoTrans,1,Q,Q,0,QQ);
	print_matrix("Checking that Q' dot Q = ", QQ );

	//Checking that QR = A
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1, Q,R,1,QR);
	print_matrix("Product of QR =" , QR);

	// ---------------------------- Ex. 1 part 2 ----------------------//
	
	fprintf(stderr, "\n2 Solving Ax = b: \n");

	gsl_vector* x  = gsl_vector_alloc(m);
	gsl_vector* b  = gsl_vector_alloc(m);
	gsl_matrix* A2 = gsl_matrix_alloc(m,m);
	gsl_matrix* A3 = gsl_matrix_alloc(m,m);
	gsl_matrix* Q2 = gsl_matrix_alloc(m,m);
	gsl_matrix* R2 = gsl_matrix_alloc(m,m);

	// make random matrix A2 and random vector b (and copy it since gsl tends to destroy it)
	magic(A2);
	gsl_matrix_memcpy(A3,A2);
	vector_magic(b);
	print_matrix("Matrix A2 of size m,m:",A2);
	print_vector("Vector b of size m:",b);
	
	//Decompose A2 into Q2 and R2
	fprintf(stderr, "\n\n  Decomposing A2 into Q2 and R2:\n\n");
	matrix_decomp(A2,Q2,R2);
	print_matrix("Q2:",Q2);
	print_matrix("R2:",R2);

	// Solve system for x:
	solve(Q2,R2,b,x);
	print_vector("Solved vector x:",x);

	// checking that Ax = b (this is the reason we saved A2 in A3):
	gsl_vector* b2 = gsl_vector_alloc(m);
	gsl_blas_dgemv(CblasNoTrans,1,A3,x,0,b2);
	print_vector("Ax =:",b2);
	print_vector("Originally calculated b",b);


	//Free the prisoners!
	gsl_matrix_free(A);	
	gsl_matrix_free(Q);	
	gsl_matrix_free(R);	
	gsl_matrix_free(QQ);	
	gsl_matrix_free(QR);	

	//------------------------Ex. 2 --------------------------//
	fprintf(stderr, "\n Calculating the inverse of the matrix A by Gram-Schmidt QR factorization \n");

	gsl_matrix* A4 = gsl_matrix_alloc(m,m);
	gsl_matrix* A5 = gsl_matrix_alloc(m,m);
	gsl_matrix* Q4 = gsl_matrix_alloc(m,m);
	gsl_matrix* R4 = gsl_matrix_alloc(m,m);
	gsl_matrix* B  = gsl_matrix_alloc(m,m);
	gsl_matrix* I  = gsl_matrix_alloc(m,m);

	magic(A4);
	gsl_matrix_memcpy(A5, A4);
	print_matrix("Random matrix A4:",A4);

	matrix_decomp(A4, Q4, R4);
	print_matrix("Calculated matrix Q4:",Q4);
	print_matrix("Calculated matrix R4:",R4);

	inverse(Q4, R4, B);
	print_matrix("Calculated matrix B", B);

	// Checking that AB = I 
	gsl_blas_dgemm(CblasNoTrans, CblasNoTrans,1,A5,B,0,I);
	print_matrix("Calculated AB = :",I);

	// Free the new prisoners!
	gsl_matrix_free(A4);
	gsl_matrix_free(Q4);
	gsl_matrix_free(R4);
	gsl_matrix_free(I);
	gsl_matrix_free(B);
	
	return 0;
}

