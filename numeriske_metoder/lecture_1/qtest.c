#include<math.h>
#include<stdio.h>
#include<assert.h>
#include<stdlib.h>

typedef struct {int n; double *x, *y, *b, *c;} qspline;
int search(int n, double *x, double z);
qspline * qspline_alloc(int n, double *x, double *y);
double qspline_spline(qspline *s, double z);
double qspline_derivative(qspline *s, double z);
double qspline_integ(qspline* s, double z);
void qspline_free(qspline *s);

int main(int argc, char const *argv[]){
	
	int n=20;
	double x[n],y[n],yp[n],yi[n];

  	for (int i = 0; i < n; i++) {
    		x[i] = 0.5*i;
    		y[i] = x[i]*x[i];
    		yp[i] = 2*x[i];
		yi[i] = pow(x[i],3)*(1.0/3.0);
		printf("%g %g %g %g\n",x[i],y[i],yp[i], yi[i]);
	}
	printf("\n\n");

	qspline * s = qspline_alloc(n,x,y);

	for(double z=x[0];z<=x[n-1];z+=0.1)
		printf("%g %g\n",z,qspline_spline(s,z));
	
	printf("\n\n");
	for(double z=x[0];z<=x[n-1];z+=0.1)
		printf("%g %g\n",z,qspline_derivative(s,z));

	printf("\n\n");
	for(double z=x[0];z<=x[n-1];z+=0.1)
		printf("%g %g\n",z,qspline_integ(s,z));

	qspline_free(s);
	return 0;
}


