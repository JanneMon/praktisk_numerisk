#include<assert.h>
#include<stdlib.h>
#include<math.h>

typedef struct {int n; double *x, *y, *b, *c;} qspline;
int search(int n, double * x, double z);
qspline * qspline_alloc(int n, double *x, double *y){
	qspline * s = (qspline*) malloc(sizeof(qspline));


	s-> n = n;
	s-> x = (double*) malloc(n*sizeof(double));
	s-> y = (double*) malloc(n*sizeof(double));
	s-> b = (double*) malloc((n-1)*sizeof(double));
	s-> c = (double*) malloc((n-1)*sizeof(double));

	for(int i = 0; i<n; i++){
		s->x[i] = x[i];
		s->y[i] = y[i];
	}

	int i;
	double dx[n-1], p[n-1];
	for (i = 0; i< n-1; i++){
		dx[i] = x[i+1] - x[i];
		p[i]  = (y[i+1]-y[i])/dx[i];
	}

	s->c[0] = 0;
	for(i = 0; i< n-2; i++){
		s->c[i+1] = (p[i+1]-p[i]-s->c[i]*dx[i])/dx[i+1];
	}
	s->c[n-2]/=2;
	for(i = n-3; i>=0; i--){
		s->c[i] = (p[i+1]-p[i]-s->c[i+1]*dx[i+1])/dx[i];
	}

	for(i = 0; i<n-1; i++){
		s->b[i] = p[i]-s->c[i]*dx[i];
	}

	return s;

}
double qspline_spline(qspline *s, double z){
	//int n = s->n;
	//assert(n >1 && z>=s->x[0]);
	
	int i = search(s->n,s->x,z);
	double spline = s->y[i] + s->b[i] * (z - s->x[i])+ s->c[i] * ((z-s->x[i])*(z-s->x[i]));
	return spline;
}

double qspline_derivative(qspline *s, double z){
	//int n = s->n;
	//assert(z >= s->x[0] && z <= s->x[s->n-1]);

	int i = search(s->n,s->x,z);
	double dsdz  = s->b[i] + 2*s->c[i]*(z-s->x[i]);
	return dsdz;	

}

double qspline_integ(qspline * s, double z){
	//int n = s->n;
	//assert(z >= s->x[0] && z <= s->x[s->n-1]);
	
	int i = search(s->n, s->x,z);
	double intsum = 0;
	for(int j = 0; j< i; j++){
		double dx = s-> x[j+1]-s->x[j];
		intsum += s-> y[j]*dx+ 0.5*s->b[j]*dx*dx + s->c[j]*dx*dx/3.0;
	}
	intsum += s->y[i]*(z-s->x[i]) + 0.5*s->b[i]*(z-s->x[i])*(z-s->x[i]) + s->c[i]*pow(z-s->x[i],3)/3.0;
	return intsum;	
}

void qspline_free(qspline *s){
	free(s->x);
	free(s->y);
	free(s->b);
	free(s->c);
	free(s);
}
