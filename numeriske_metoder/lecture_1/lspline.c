#include<math.h>
#include<stdio.h>
int search(int n, double *x, double z);

double linterp(int n, double *x, double *y, double z){
	int i = search(n,x,z);
	double p = (y[i+1]-y[i])/(x[i+1]-x[i]);
	double deltax = z - x[i];
	double s = y[i] + p*deltax;
	return s;

}

double linterp_integ(int n, double *x, double *y, double z){
	int target = search(n,x,z);
	double integral = 0;

	//calculating the integral (analytically) for all values less than the target
	for(int i = 0; i <target; i++){
		double deltax = x[i + 1] - x[i];
		double p = (y[i + 1]-y[i])/deltax;
		integral += y[i]*deltax + 0.5*p*deltax*deltax;
	}

	//calculating the integral at the exact point at the target, and adding this result to the integrated values calculated in the forloop
	double deltax = z - x[target];
	double p = (y[target + 1]-y[target])/(x[target + 1]-x[target]);
	integral += y[target]*deltax + 0.5*p*deltax*deltax;
	return integral;
}
