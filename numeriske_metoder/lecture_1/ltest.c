#include<math.h>
#include<stdio.h>

int search(int n, double* x, double z);
double linterp(int n, double* x, double* y, double z);
double linterp_integ(int n, double* x, double *y, double z);


int main(void){
	
	int n = 20;

	//defines x vector and corresponding y-values in a vector og sizes n
	double x[n];
	double y[n];

	//defines test target vector with size n*10
	double xtarget[10*n];
	double ytarget[10*n];
	
	//defines interpolated vectors
	double yintp[10*n];
	double yintpintg[10*n];

	//define test 
	double yinteg[10*n];

	//runs forlopo to fill entrances in x and y vectors of size 20, y is testfunction x²
	for(int i = 0; i <n; i++){
		x[i] = 0.5*i;
		y[i] = x[i]*x[i];
	}

	//runs forloop to fill entrances in target vectors (200 entrances)
	for(int i = 0; i <10*n; i++){
	xtarget[i] = 0.05*i;
	ytarget[i] = pow(xtarget[i],2);
	yinteg[i] = pow(xtarget[i],3)/3.0;

		
		if(xtarget[i]<x[n-1]){
		yintp[i] = linterp(n,x,y,xtarget[i]);
		yintpintg[i] = linterp_integ(n,x,y,xtarget[i]);
		printf("%8g \t %8g \t %8g \t %8g \t %8g \t\n",xtarget[i], ytarget[i],yintp[i],yinteg[i],yintpintg[i]);
		}
	}

return 0;
}
