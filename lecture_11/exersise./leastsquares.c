#include<stdlib.h>
#include<stdio.h>
#include<gsl/gsl_multimin.h>
#include<gsl/gsl_vector.h>
#include<math.h>

typedef struct {int n; double *t, *y, *e;} experimental_data;

double minimizer(const gsl_vector *x, void* params){
	double A  = gsl_vector_get(x,0);
	double T  = gsl_vector_get(x,1);
	double B  = gsl_vector_get(x,2);
	experimental_data *p = (experimental_data*) params;
	int n     = p->n;
	double *t = p->t;
	double *y = p->y;
	double *e = p->e;
	double sum= 0;
	double f(double t){return A*exp(-(t)/T)+B;} //returns the function that the least squares fit should be done on
	for(int i = 0; i<n;i++) sum += pow ((f(t[i])-y[i])/e[i],2);
	return sum;
}

int main(){
	double t[] = {0.47,1.41,2.36,3.30,4.24,5.18,6.13,7.07,8.01,8.95};
	double y[] = {5.49,4.08,3.54,2.61,2.09,1.91,1.55,1.47,1.45,1.25};
	double e[] = {0.26,0.12,0.27,0.10,0.15,0.11,0.13,0.07,0.15,0.09};
	int n = sizeof(t)/sizeof(t[0]);

	experimental_data data;
	data.n = n;
	data.t = t;
	data.y = y;
	data.e = e;

	const int dim = 3;
	gsl_multimin_fminimizer *Mini
		= gsl_multimin_fminimizer_alloc(gsl_multimin_fminimizer_nmsimplex2,dim);

	gsl_multimin_function func;
	func.f = minimizer;
	func.n = dim;
	func.params = (void*)&data;

	gsl_vector* start = gsl_vector_alloc(dim);
	gsl_vector* step  = gsl_vector_alloc(dim);

	double start_guess[3] = {4,3,1};
	double step_guess[3]={1,1,1};

	gsl_vector_set(start,0,start_guess[0]);
	gsl_vector_set(start,1,start_guess[1]);
	gsl_vector_set(start,2,start_guess[2]);

	gsl_vector_set(step,0,step_guess[0]);
	gsl_vector_set(step,1,step_guess[1]);
	gsl_vector_set(step,2,step_guess[2]);

	gsl_multimin_fminimizer_set(Mini,&func,start,step);

	int iteration = 0;

	do{
		iteration++;
		gsl_multimin_fminimizer_iterate(Mini);
		int result = gsl_multimin_test_size(Mini->size, 1e-2);
		if(result == GSL_SUCCESS) break;
	}while(1);

	double A = gsl_vector_get(Mini->x,0);
	double T = gsl_vector_get(Mini->x,1);
	double B = gsl_vector_get(Mini->x,2);
	
	double f(double t){return A*exp(-(t)/T)+B;} 

	for(double x = t[0];x<t[n-1]+0.1;x+=0.05)
		printf(" %g %g\n",x,f(x));
	fprintf(stderr,"A=%g \t T=%g \t  B=%g \t iteration = %i \n",A,T,B,iteration);

	gsl_vector_free(start);
	gsl_vector_free(step);
	gsl_multimin_fminimizer_free(Mini);

	return 0;
}
