#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<stdlib.h>
#include<gsl/gsl_multimin.h>

double minfunc(const gsl_vector* vec, void* params){
	double x = gsl_vector_get(vec,0);
	double y = gsl_vector_get(vec,1);
	
	return pow(1-x,2) + 100*pow(y-x*x,2);	
}

int main(int argc, const char *argv[]){
	//defining dimensions//	
	const int dim = 2;
	
	//defining minimizer type and workspace allocation//
	gsl_multimin_fminimizer* Mini
	      	= gsl_multimin_fminimizer_alloc(gsl_multimin_fminimizer_nmsimplex,2);

	//defining function to solve//
	gsl_multimin_function F;
	F.f = minfunc;
	F.n = dim;
	F.params = NULL;
	

	//allocate space for initial guess vector and set entries//
	double s1[2] = {2,2}; double s2[2] = {0.1,0.1};

	gsl_vector* start = gsl_vector_alloc(dim);
	gsl_vector_set(start, 0, s1[0]);
	gsl_vector_set(start, 1, s1[1]);
	
	gsl_vector* step = gsl_vector_alloc(dim);
	gsl_vector_set(step,0,s2[0]);
	gsl_vector_set(step,1,s2[1]);

	gsl_multimin_fminimizer_set(Mini,&F, start,step );

	//defining parameters for iteration
	int result, i=0;
	   // ulimit = 1000;
	// the function stars searching for a minimum//
	do{ 
		i++;
		result = gsl_multimin_fminimizer_iterate(Mini);
		double x = gsl_vector_get(Mini->x,0);
		double y = gsl_vector_get(Mini->x,1);
		fprintf(stderr,"%g %g\n",x,y);
		result = gsl_multimin_test_size(Mini->size,1e-2);
		if(result == GSL_SUCCESS)break;
	}while(1);
//	}while(result == GSL_CONTINUE && i < ulimit);
	
	double x = gsl_vector_get(Mini->x,0);
	double y = gsl_vector_get(Mini->x,1);
	
	
	//free the allocated memory
	gsl_vector_free(start);
	gsl_vector_free(step);
	gsl_multimin_fminimizer_free(Mini);
	
	printf("xmin, ymin = ( %g %g), found with %i iterations\n",x,y,i);
	
	return 0;
}

/*int main(){
	//double minfinder(double);
	for(double x = 0; x<17;x+=0.1)
		printf("%lg\t %lg\n",x,minfinder(x));
	
	return 0;
}*/
