//POINTERS; ARRAYS; STRUCTURES//

#include<stdio.h>
#include<math.h>

void bar(double* x){
	*x=777;
}
int main(){
	double x = 0;
	printf("pointer to x is = %u\n",*&x);
	bar(&x);
	printf("x=%g\n",x);
}


