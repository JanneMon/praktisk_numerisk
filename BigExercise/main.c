#include<stdio.h>
#include<math.h>
#include<gsl/gsl_integration.h>


double cosfunc(double z, void* params){
	double F = 1.0/sqrt(1.0-(z*z));
	return F;
}

int main(){
	gsl_function func;
	func.function= &cosfunc;
	func.params=NULL;
	
	double res;
	double err;
	double acc=1e-6;
	double eps=1e-6;
	size_t limit = 100;
	double x = 0.97;

	gsl_integration_workspace* workspace =
		gsl_integration_workspace_alloc(limit);	

	for(double x = 0.01; x<0.99; x+=0.02){
		double boundaries[2] = {x,1};
		gsl_integration_qags(&func,boundaries[0], boundaries[1],acc, eps, limit, workspace, &res, &err);
		printf("%g \t %g \t %g\n",x,res,acos(x));
	}

	fprintf(stderr,"The integral of the function 1/sqrt(1-z^2) from 0.99 to 1 is %g\n",res);
	fprintf(stderr,"And the function arccos(x) = %g\n",acos(x));
		
	gsl_integration_workspace_free(workspace);

	return 0;
}
